//
//  Score.m
//  sumUps
//
//  Created by Audouin d'Aboville on 12/07/2014.
//  Copyright (c) 2014 Audouin d'Aboville. All rights reserved.
//

#import "Score.h"

@interface Score ()

@end

@implementation Score

- (void)viewDidLoad 
{
    [super viewDidLoad];
    
    NSLog(@"Passage vue score ! ");
    
    //  Espace publicitaire & google analytics
    
    [_Pub_Score loadRequest:[NSURLRequest requestWithURL: [NSURL URLWithString : @"http://www.ad-inc.fr/Autres/SumUp/score.html"]]];
    
    NSLog(@"Analytics init ok");
    

    // ---------------------------------------
    // Lecture du fichier de sauvegarde
    
    NSString *chemin = [[self applicationDocumentsDirectory].path
                        stringByAppendingPathComponent:@"Data.txt"];
    NSString *Var_temp1 = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
    
    NSLog(@"Resultat txt: %@", Var_temp1);
    
    if (Var_temp1==NULL)
    {
        Var_temp1=@"0";
    }
    
    _Label_BestScore.text = Var_temp1;
    
    int bstscore = [Var_temp1 intValue];
    
    if (bstscore>=0 && bstscore<10)
    {
        _lvl4.image = nil; // Disapear all
        _lvl3.image = nil;
        _lvl2.image = nil;
    }
    
    else if (bstscore>=10 && bstscore <15)
    {
        _silver1.image = nil;
        _lvl3.image = nil;
        _lvl2.image = nil;
    }
    
    else if (bstscore >=15 && bstscore <=24)
    {
        _silver1.image = nil;
        _silver2.image = nil;
        _lvl2.image = nil;
    }
    
    else
    {
        _silver3.image = nil;
        _silver1.image = nil;
        _silver2.image = nil;// Disapear nothing
    }
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)BouttonFacebook:(id)sender
{
    NSLog(@"Facebook");
    
    // Vérification des "Achivement"
    [self updateAchievements:@"Facebook"];
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController * fbSheetOBJ = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        NSString *chemin = [[self applicationDocumentsDirectory].path
                            stringByAppendingPathComponent:@"Data.txt"];
        NSString *Var_temp1 = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
        NSString *msg = [NSString stringWithFormat:@"I reach the level %@ on SumUp ! Join me !", Var_temp1];
        
        [fbSheetOBJ setInitialText:msg];
        [fbSheetOBJ addURL:[NSURL URLWithString:@"http://ad-inc.pagesperso-orange.fr/SumUp/"]];
        [fbSheetOBJ addImage:[UIImage imageNamed:@"logoSumUp.png"]];
        
        [self presentViewController:fbSheetOBJ animated:YES completion:Nil];
    }
}

- (IBAction)BouttonTwitter:(id)sender
{
    NSLog(@"Twitter");
    
    // Vérification des "Achivement"
    [self updateAchievements:@"Twitter"];
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheetOBJ = [SLComposeViewController
                                                  composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        NSString *chemin = [[self applicationDocumentsDirectory].path
                            stringByAppendingPathComponent:@"Data.txt"];
        NSString *Var_temp1 = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
        NSString *msg = [NSString stringWithFormat:@"I reach the level %@ on SumUp ! Join me ! http://ad-inc.pagesperso-orange.fr/SumUp/", Var_temp1];
        
        [tweetSheetOBJ setInitialText:msg];
        [self presentViewController:tweetSheetOBJ animated:YES completion:nil];
    }
}

- (IBAction)show_leaderboard:(id)sender
{
    [self showLeaderboardAndAchievements:YES];
  // [self resetAchievements];
}

-(void)updateAchievements:(NSString*)wich_button
{
    NSLog(@"updateAchivement");
    
    NSString *achievementIdentifier;
    float progressPercentage = 0.0;
    BOOL progressInLevelAchievement = NO;
    
    GKAchievement *levelAchievement = nil;
    GKAchievement *scoreAchievement = nil;

    // Vérification préalable
    
    NSString *contenu; // Contient le contenu du fichier "first_Twitter.txt"
    NSString *chemineau = [[self applicationDocumentsDirectory].path
                           stringByAppendingPathComponent:@"first_Twitter.txt"];
    contenu = [NSString stringWithContentsOfFile:chemineau encoding:NSUTF8StringEncoding error:nil];
    
    NSString *contenu2; // Contient le contenu du fichier "first_Facebook.txt"
    NSString *chemineau2 = [[self applicationDocumentsDirectory].path
                           stringByAppendingPathComponent:@"first_Facebook.txt"];
    contenu2 = [NSString stringWithContentsOfFile:chemineau2 encoding:NSUTF8StringEncoding error:nil];
    
    if ([wich_button  isEqual: @"Twitter"])
    {
        if ([contenu isEqual:@"0"] || contenu == NULL)
        {
            NSLog(@"Twitter achivement ON");
            [GKNotificationBanner showBannerWithTitle:@"Score on Twitter !" message:@"Thanx for sharing, you earn 25 points" completionHandler:nil];
            progressPercentage = 100;
            achievementIdentifier = @"SUMUPS_Achivement_Twitter";
            progressInLevelAchievement = YES;
        
            // Création du fichier first_twitter
            NSString *path = [[self applicationDocumentsDirectory].path
                          stringByAppendingPathComponent:@"first_Twitter.txt"];
            bool rendu = [@"1" writeToFile:path atomically:YES
                              encoding:NSUTF8StringEncoding error:nil];
            NSLog(@"Création du fichier first Twitter : %d", rendu);
        }
    }
    
    else if ([wich_button  isEqual: @"Facebook"])
    {
        if ([contenu2 isEqual:@"0"] || contenu2 == NULL)
        {
            NSLog(@"Facebook achivement ON");
            [GKNotificationBanner showBannerWithTitle:@"Score on Facebook !" message:@"Thanx for sharing, you earn 50 points" completionHandler:nil];
            progressPercentage = 100;
            achievementIdentifier = @"SUMUPS_Achivement_Facebook";
            progressInLevelAchievement = YES;
        
            // Création du fichier first_facebook
            NSString *path = [[self applicationDocumentsDirectory].path
                          stringByAppendingPathComponent:@"first_Facebook.txt"];
            bool rendu = [@"1" writeToFile:path atomically:YES
                              encoding:NSUTF8StringEncoding error:nil];
            NSLog(@"Création du fichier first Facebook : %d", rendu);
        }
    }
    
    if (progressInLevelAchievement)
    {
        levelAchievement = [[GKAchievement alloc] initWithIdentifier:achievementIdentifier];
        levelAchievement.percentComplete = progressPercentage;
    }
    
    scoreAchievement = [[GKAchievement alloc] initWithIdentifier:achievementIdentifier];
    scoreAchievement.percentComplete = progressPercentage;
    
    NSArray *achievements = (progressInLevelAchievement) ? @[levelAchievement, scoreAchievement] : @[scoreAchievement];
    
    [GKAchievement reportAchievements:achievements withCompletionHandler:^(NSError *error) {
    }];
}

-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [gameCenterViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)showLeaderboardAndAchievements:(BOOL)shouldShowLeaderboard
{
    GKGameCenterViewController *gcViewController = [[GKGameCenterViewController alloc] init];
    
    gcViewController.gameCenterDelegate = self;
    
    if (shouldShowLeaderboard)
    {
        gcViewController.viewState = GKGameCenterViewControllerStateLeaderboards;
        gcViewController.leaderboardIdentifier = @"SUMUPS_LEADERBOARDS";
    }
    else
    {
        gcViewController.viewState = GKGameCenterViewControllerStateAchievements;
    }
    
    [self presentViewController:gcViewController animated:YES completion:nil];
}

-(void)resetAchievements
{
    [GKAchievement resetAchievementsWithCompletionHandler:^(NSError *error) {
        if (error != nil) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }];
}

- (IBAction)backtomenu:(id)sender
{
    NSLog(@"Passage vers menu");
    
    Menu *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    
    [self presentViewController:second_view animated:NO completion:nil];
}

@end