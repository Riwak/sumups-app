//
//  View_LOOSE_pay.h
//  SumUps
//
//  Created by Audouin d'Aboville on 22/10/2014.
//  Copyright (c) 2014 Audouin d'Aboville. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import "Solution.h"
#import "ViewController.h"
#import "Menu.h"

@interface View_LOOSE_pay : UIViewController
{
    int level;
    int life;
}

@property (weak, nonatomic) IBOutlet UILabel *display_lvl;

@property (weak, nonatomic) IBOutlet UIImageView *no_life1;
@property (weak, nonatomic) IBOutlet UIImageView *no_life2;
@property (weak, nonatomic) IBOutlet UIImageView *no_life3;

@property (weak, nonatomic) IBOutlet UIImageView *life1;
@property (weak, nonatomic) IBOutlet UIImageView *life2;
@property (weak, nonatomic) IBOutlet UIImageView *life3;

- (void)updateAchievements:(NSString*)wich_button;

- (void)setMy_Score: (int)lvl;

- (IBAction)twitter_share:(id)sender;
- (IBAction)Facebook_share:(id)sender;

- (IBAction)restart:(id)sender;
- (IBAction)cancel:(id)sender;

@end