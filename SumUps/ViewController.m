//
//  ViewController.m
//  sumUps
//
//  Created by Audouin d'Aboville on 12/07/2014.
//  Copyright (c) 2014 Audouin d'Aboville. All rights reserved.
//
//  Optimisation iPhone 5c iOS 9.1.0
//  ---------------------------------
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //  Espace publicitaire & google analytics
    
    [_Pub_Game loadRequest:[NSURLRequest requestWithURL: [NSURL URLWithString : @"http://www.ad-inc.fr/Autres/SumUp/game.html"]]];
    
    NSLog(@"Analytics init ok");

    
    // Séquence s'initialisation
    
    win=false;
    
    if (lvl == NULL) // Warning integer/pointer [FONCTIONNE]
    {
    lvl=1;
    NSLog(@"init lvl");
    }
    
    time=60;
    
    Nb_combinaison_max=9;
    Mes_combinaisons=0;
    
    
    // ----------------------------------
    // Génération des nombres
    
    if (lvl < 5)
    {
         time = 60;
         low_bound = -10;
         high_bound = 10;
        
        if (lvl >2 && lvl <5)
        {
             time = 25;
             low_bound = -10;
             high_bound = 10;
        }
    }
    
    else if (lvl>=5 && lvl <10)
    {
        time=15;
         low_bound = -10;
         high_bound = 10;
    }
    
    else if (lvl >=10 && lvl <=15)
    {
        time = (5-lvl)+20; // 15,14,13,12,11
         low_bound = -lvl; // 10,11,12,13,14,15
        high_bound = lvl;
    }
    
    else // lvl > 15
    {
        time = 10;
       low_bound = -lvl;
         high_bound = lvl;
    }
    
    // Background par niveau
    
UIColor *colour1 = [[UIColor alloc]initWithRed:254.0/255.0 green:233.0/255.0 blue:218.0/255.0 alpha:1.0];
UIColor *colour2 = [[UIColor alloc]initWithRed:221.0/255.0 green:240.0/255.0 blue:175.0/255.0 alpha:1.0];
UIColor *colour3 = [[UIColor alloc]initWithRed:189.0/255.0 green:243.0/255.0 blue:231.0/255.0 alpha:1.0];
UIColor *colour4 = [[UIColor alloc]initWithRed:233.0/255.0 green:204.0/255.0 blue:242.0/255.0 alpha:1.0];
UIColor *colour5 = [[UIColor alloc]initWithRed:227.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1.0];

    
    // Génération des nombres aléatoire
    
    do
    {
        NSLog(@"Test new security loop");
        
        Button1 = (((float)arc4random()/0x100000000)*(high_bound-low_bound)+low_bound);
        Button2 = (((float)arc4random()/0x100000000)*(high_bound-low_bound)+low_bound);
        Button3 = (((float)arc4random()/0x100000000)*(high_bound-low_bound)+low_bound);
    
        Button4 = (((float)arc4random()/0x100000000)*(high_bound-low_bound)+low_bound);
        Button5 = (((float)arc4random()/0x100000000)*(high_bound-low_bound)+low_bound);
        Button6 = (((float)arc4random()/0x100000000)*(high_bound-low_bound)+low_bound);
    
        Button7 = (((float)arc4random()/0x100000000)*(high_bound-low_bound)+low_bound);
        Button8 = (((float)arc4random()/0x100000000)*(high_bound-low_bound)+low_bound);
        Button9 = (((float)arc4random()/0x100000000)*(high_bound-low_bound)+low_bound);
    
        // ------------------------------------- */
        // Generation de l'objectif & background
    
        int tableau[10];
    
        tableau[0]= Button1;
        tableau[1]= Button2;
        tableau[2]= Button3;
        tableau[3]= Button4;
        tableau[4]= Button5;
        tableau[5]= Button6;
        tableau[6]= Button7;
        tableau[7]= Button8;
        tableau[8]= Button9;
        tableau[9]= 0;
        
        transfert_solution[0]=0;
        transfert_solution[1]=0;
        transfert_solution[2]=0;
        transfert_solution[3]=0;
        transfert_solution[4]=0;
        transfert_solution[5]=0;
        
        int cpt;
    
        if (lvl < 5) // Niveau 1 Algo(1) [3]touch
        {
        self.view.backgroundColor = colour1;
        
        int tableau_temp[3];
        
        do
        {
            for (cpt=0; cpt<=2; cpt++)
            {
                int choix = arc4random() % 8;
                tableau_temp[cpt]=tableau[choix];
                NSLog(@"Choix : %d", choix);
                NSLog(@"tableau[choix] : %d", tableau[choix]);
                NSLog(@"cpt : %d",cpt);
            }
            
            NSLog(@"------");
            
        } while (tableau_temp[0] == tableau_temp[1] || tableau_temp[0] == tableau_temp[2] || tableau_temp[1] == tableau_temp[2]); // je ne le sens pas pour la boucle infini
            
            Objectif=tableau_temp[0]+tableau_temp[1]+tableau_temp[2];
        
        // Affichage debuggage d'une solution possible
        NSLog(@"Target : %d", Objectif);
        NSLog(@"1) = %d", tableau_temp[0]);
        NSLog(@"2) = %d", tableau_temp[1]);
        NSLog(@"3) = %d", tableau_temp[2]);
        // Fin du debuggage
            
        transfert_solution[0] = tableau_temp[0];
        transfert_solution[1] = tableau_temp[1];
        transfert_solution[2] = tableau_temp[2];
    }
    
        else if (lvl>=5 && lvl <10) // Niveau 2 Algo (2) [4]touch
        {
        NSLog(@"Level up !");
        
        self.view.backgroundColor = colour2; // Changement background
        
        int tableau_temp[4];
        
        for (cpt=0; cpt<=3; cpt++)
        {
            int choix = 9;
            while (tableau[choix] == 0)
            {
                choix = arc4random() % 8;
            }
            tableau_temp[cpt]=tableau[choix];
            tableau[choix]=0;
        }
        
        Objectif=tableau_temp[0]+tableau_temp[1]+tableau_temp[2]+tableau_temp[3];
        
        // Affichage debuggage d'une solution possible
        NSLog(@"Target : %d", Objectif);
        NSLog(@"1) = %d", tableau_temp[0]);
        NSLog(@"2) = %d", tableau_temp[1]);
        NSLog(@"3) = %d", tableau_temp[2]);
        NSLog(@"4) = %d", tableau_temp[3]);
        // Fin du debuggage
            
            transfert_solution[0] = tableau_temp[0];
            transfert_solution[1] = tableau_temp[1];
            transfert_solution[2] = tableau_temp[2];
            transfert_solution[3] = tableau_temp[3];
    }
    
        else if (lvl >= 10 && lvl <= 15) // Niveau 3 Algo (2) [5]touch
        {
        NSLog(@"Level up !");
        
        self.view.backgroundColor = colour3; // Changement background
        
        int tableau_temp[5];
        
        for (cpt=0; cpt<=4; cpt++)
        {
            int choix = 9;
            while (tableau[choix] == 0)
            {
                choix = arc4random() % 8;
            }
            tableau_temp[cpt]=tableau[choix];
            tableau[choix]=0;
        }
        
        Objectif=tableau_temp[0]+tableau_temp[1]+tableau_temp[2]+tableau_temp[3]+tableau_temp[4];
        
        // Affichage debuggage d'une solution possible
        NSLog(@"Target : %d", Objectif);
        NSLog(@"1) = %d", tableau_temp[0]);
        NSLog(@"2) = %d", tableau_temp[1]);
        NSLog(@"3) = %d", tableau_temp[2]);
        NSLog(@"4) = %d", tableau_temp[3]);
        NSLog(@"5) = %d", tableau_temp[4]);
        // Fin du debuggage
            
            transfert_solution[0] = tableau_temp[0];
            transfert_solution[1] = tableau_temp[1];
            transfert_solution[2] = tableau_temp[2];
            transfert_solution[3] = tableau_temp[3];
            transfert_solution[4] = tableau_temp[4];
    }
    
        else // Niveau 4 >15 Algo(2) [6]touch
        {
        NSLog(@"Level up !");
        
        self.view.backgroundColor = colour4; // Changement background
        
        int tableau_temp[6];
        
        for (cpt=0; cpt<=5; cpt++)
        {
            int choix = 9;
            while (tableau[choix] == 0)
            {
                choix = arc4random() % 8;
            }
            tableau_temp[cpt]=tableau[choix];
            tableau[choix]=0;
        }
        
        Objectif=tableau_temp[0]+tableau_temp[1]+tableau_temp[2]+tableau_temp[3]+tableau_temp[4]+tableau_temp[5];
        
        // Affichage debuggage d'une solution possible
        NSLog(@"Target : %d", Objectif);
        NSLog(@"1) = %d", tableau_temp[0]);
        NSLog(@"2) = %d", tableau_temp[1]);
        NSLog(@"3) = %d", tableau_temp[2]);
        NSLog(@"4) = %d", tableau_temp[3]);
        NSLog(@"5) = %d", tableau_temp[4]);
        NSLog(@"6) = %d", tableau_temp[5]);
        // Fin du debuggage
            
            transfert_solution[0] = tableau_temp[0];
            transfert_solution[1] = tableau_temp[1];
            transfert_solution[2] = tableau_temp[2];
            transfert_solution[3] = tableau_temp[3];
            transfert_solution[4] = tableau_temp[4];
            transfert_solution[5] = tableau_temp[5];
    }
        
        var_zero_test = Button1*Button2*Button3*Button4*Button5*Button6*Button7*Button8*Button9;
    
    }while (Objectif==Button1 || Objectif==Button2 || Objectif==Button3 || Objectif==Button4 || Objectif==Button5 || Objectif==Button6 || Objectif==Button7 || Objectif==Button8 || Objectif==Button9 || Objectif==0 || var_zero_test==0);

    // ---------------------------------------
    // Séquence de convertion pour l'affichage
    
    NSString *variable10 = [@(lvl) stringValue];
    _level.text = variable10;
    
    [self check_score];
    
    variable10 = [@(Objectif) stringValue];
    _goal.text = variable10;
    
    variable10 = [@(time) stringValue];
    _time.text = variable10;
    
    Actuel=0;
    variable10 = [@(Actuel) stringValue];
    _actuel.text = variable10;
    
    statut_button1=0;
    NSString *variable = [@(Button1) stringValue];
    [_Label__Bouton1 setTitle: variable forState:UIControlStateNormal];
    
    statut_button2=0;
    variable = [@(Button2) stringValue];
    [_Label__Bouton2 setTitle: variable forState:UIControlStateNormal];
    
    statut_button3=0;
    variable = [@(Button3) stringValue];
    [_Label__Bouton3 setTitle: variable forState:UIControlStateNormal];
    
    statut_button4=0;
    variable = [@(Button4) stringValue];
    [_Label__Bouton4 setTitle: variable forState:UIControlStateNormal];
    
    statut_button5=0;
    variable = [@(Button5) stringValue];
    [_Label__Bouton5 setTitle: variable forState:UIControlStateNormal];
    
    statut_button6=0;
    variable = [@(Button6) stringValue];
    [_Label__Bouton6 setTitle: variable forState:UIControlStateNormal];
    
    statut_button7=0;
    variable = [@(Button7) stringValue];
    [_Label__Bouton7 setTitle: variable forState:UIControlStateNormal];
    
    statut_button8=0;
    variable = [@(Button8) stringValue];
    [_Label__Bouton8 setTitle: variable forState:UIControlStateNormal];
    
    statut_button9=0;
    variable = [@(Button9) stringValue];
    [_Label__Bouton9 setTitle: variable forState:UIControlStateNormal];
    
    // Colorisation de l'affichage
    
    if (lvl >= 20)
    {
       self.view.backgroundColor = colour5;
    }
    
UIColor *colourButton = [[UIColor alloc]initWithRed:0.0/255.0 green:51.0/255.0 blue:102.0/255.0 alpha:1.0];
    
    [_Label__Bouton1 setTintColor: colourButton];
    [_Label__Bouton2 setTintColor: colourButton];
    [_Label__Bouton3 setTintColor: colourButton];
    [_Label__Bouton4 setTintColor: colourButton];
    [_Label__Bouton5 setTintColor: colourButton];
    [_Label__Bouton6 setTintColor: colourButton];
    [_Label__Bouton7 setTintColor: colourButton];
    [_Label__Bouton8 setTintColor: colourButton];
    [_Label__Bouton9 setTintColor: colourButton];
    [_time setTextColor: [UIColor blackColor]];
    
    [_Label__Bouton1 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
    [_Label__Bouton2 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
    [_Label__Bouton3 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
    [_Label__Bouton4 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
    [_Label__Bouton5 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
    [_Label__Bouton6 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
    [_Label__Bouton7 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
    [_Label__Bouton8 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
    [_Label__Bouton9 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
    
    // Initialisation de la classe boucle de jeu
    
    // INIT FONCTION DEBUG
   // main = [NSTimer scheduledTimerWithTimeInterval: 1.00 target: self selector:@selector(boucleJeu) userInfo:nil repeats:YES]; // Execution de la méthode boucle de jeu tt les secondes
    
    // DEBUG FONCTION OUTPUT (180 - 2minutes working after background mode)
    UIBackgroundTaskIdentifier bgTask = UIBackgroundTaskInvalid;
    UIApplication *app = [UIApplication sharedApplication];
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
    }];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self   selector:@selector(boucleJeu) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    
    
    // ------------------------------------------------------
    // Affichage des règles en début de partie (UNIQUE FIRST)
    
    NSString *contenu; // Contient le contenu du fichier "first_rules.txt"
    
    NSString *chemineau = [[self applicationDocumentsDirectory].path
                           stringByAppendingPathComponent:@"first_rules.txt"];
    contenu = [NSString stringWithContentsOfFile:chemineau encoding:NSUTF8StringEncoding error:nil];
    
    if ([contenu isEqual:@"0"] || contenu == NULL)
    {
        // Création du fichier first
    
        NSString *path = [[self applicationDocumentsDirectory].path
                      stringByAppendingPathComponent:@"first_rules.txt"];
        bool rendu = [@"1" writeToFile:path atomically:YES
                          encoding:NSUTF8StringEncoding error:nil];
    
        NSLog(@"Création du fichier first rules : %d", rendu);
    
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Help & clues !" message:@"You have only one minute to reach the goal by mathematics and your head. You can use different combinaisons." delegate:self cancelButtonTitle:@"Continue" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)audio_tools:(NSString*)which_one
{
    NSLog(@"Sound !");
    
    if ([which_one isEqual:@"loose"])
    {
        SystemSoundID loose;

        AudioServicesCreateSystemSoundID(CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR("loose"), CFSTR("caf"), NULL), &loose);
        AudioServicesPlaySystemSound(loose);
    }
    
    else if ([which_one isEqual:@"win"])
    {
        SystemSoundID winn;

        AudioServicesCreateSystemSoundID(CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR("winn"), CFSTR("caf"), NULL), &winn);
        AudioServicesPlaySystemSound(winn);
    }
    
    else if ([which_one isEqual:@"clique"])
    {
        SystemSoundID clique;

        AudioServicesCreateSystemSoundID(CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR("Clique"), CFSTR("caf"), NULL), &clique);
        AudioServicesPlaySystemSound(clique);
    }
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) boucleJeu
{
    // Passage txt-->nb
    int time_temp;
    time_temp= [_time.text intValue];
    
    if (time_temp==10)
    {
        // Changement de couleur
        [_time setTextColor: [UIColor redColor]];
        NSLog(@"Changement de couleurs");
    }
    
    else if (time_temp<=0)
    {
        [timer invalidate];
        [self JaiPerdu];
        time=60;
        lvl=1;
    }
        // Incrémentation
        time_temp = time_temp-1;
    
        // Passage nb-->txt
        NSString *variable;
        variable = [@(time_temp) stringValue];
    
        // Enregistrement sur le controller
        _time.text = variable;
        
        // NSLog(@"Adresse en mémoire de Time : %p", &main);
}

- (void) Set_lvl:(int)level
{
    lvl=level;
}

- (void) JaiPerdu
{
    NSLog(@"Perdu !");
    
    [self audio_tools:@"loose"];
     
     View_LOOSE *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"View_LOOSE"];
     
     [self presentViewController:second_view animated:NO completion:nil];
     
     [second_view setMy_Score:lvl];
     [second_view setMy_Goal:Objectif];
     [second_view setMy_Solution:transfert_solution];
     [second_view setMy_Board:Button1 andNum2:Button2 andNum3:Button3 andNum4:Button4 andNum5:Button5 andNum6:Button6 andNum7:Button7 andNum8:Button8 andNum9:Button9];
}

- (void) check_score
{
    // ---------------------------------------
    // Enregistrement et lecture du score
    // Lecture
    
    NSString *Var_temp1; // Contient le contenu du fichier score
    
    NSString *chemin = [[self applicationDocumentsDirectory].path
                        stringByAppendingPathComponent:@"Data.txt"];
    Var_temp1 = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
    
    if (Var_temp1==NULL)
    {
        Var_temp1=@"0";
    }
    
    int test = [Var_temp1 intValue];// Conversion du contenu data.txt en int
    
    NSLog(@"Comparaison recup : %d", test);
    NSLog(@"Comparaison lvl : %d", lvl);
    
    // Enregistrement si supp comparaison de chaine de caractère
    if (test < lvl)
    {
        NSString* texte = _level.text;
        
        // Ecriture
        NSString *path = [[self applicationDocumentsDirectory].path
                          stringByAppendingPathComponent:@"Data.txt"];
        bool rendu = [texte writeToFile:path atomically:YES
                               encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"Score upgrade (bool), %d, %@", rendu, texte);
        
        // Envois des données sur Game-Center
        [self reportScore];
        
        // Vérification des "Achivement"
        [self updateAchievements];
    }
}

-(void) updateAchievements
{
    NSLog(@"updateAchivement");
    
    NSString *achievementIdentifier;
    float progressPercentage = 0.0;
    BOOL progressInLevelAchievement = NO;
    
    GKAchievement *levelAchievement = nil;
    GKAchievement *scoreAchievement = nil;
    
        if (lvl == 5)
        {
            NSLog(@"1er achivement ON");
            
            [GKNotificationBanner showBannerWithTitle:@"You reached level 5 !" message:@"You earn the first award ! Congratulations !" completionHandler:nil];
            
            progressPercentage = 100;
            achievementIdentifier = @"SUMUPS_Achivement_lvl1";
            progressInLevelAchievement = YES;
        }
        else if (lvl == 10)
        {
            NSLog(@"2er achivement ON");
            
            [GKNotificationBanner showBannerWithTitle:@"You reached level 10 !" message:@"You earned the second award on three. Stay alive !" completionHandler:nil];
            
            progressPercentage = 100;
            achievementIdentifier = @"SUMUPS_Achivement_lvl2";
            progressInLevelAchievement = YES;
        }
    
        else if (lvl == 16)
        {
            NSLog(@"3er achivement ON");
            
            [GKNotificationBanner showBannerWithTitle:@"You reached the level 16" message:@"You have almost won all the medals ! You are the best !" completionHandler:nil];
            
            progressPercentage = 100;
            achievementIdentifier = @"SUMUPS_Achivement_lvl3";
            progressInLevelAchievement = YES;
        }
    
        else if (lvl == 25)
        {
            NSLog(@"Master achivement ON");
            
            [GKNotificationBanner showBannerWithTitle:@"Master of SumUps !" message:@"Amazing ! You are officialy a SumUps Master ! Congratulation :)" completionHandler:nil];
            
            progressPercentage = 100;
            achievementIdentifier = @"SUMUPS_ACHIEVEMENT_MASTER";
            progressInLevelAchievement = YES;
        }
    
        else if (lvl == 20)
        {
            NSLog(@"last achivement ON");
            
            [GKNotificationBanner showBannerWithTitle:@"Chief !" message:@"Incredible ! You are almost unique !" completionHandler:nil];
            
            progressPercentage = 100;
            achievementIdentifier = @"SuMUPS_Achivement_Bonus";
            progressInLevelAchievement = YES;
        }
    
    if (progressInLevelAchievement)
    {
        levelAchievement = [[GKAchievement alloc] initWithIdentifier:achievementIdentifier];
        levelAchievement.percentComplete = progressPercentage;
    }
    
    scoreAchievement = [[GKAchievement alloc] initWithIdentifier:achievementIdentifier];
    scoreAchievement.percentComplete = progressPercentage;
    
    NSArray *achievements = (progressInLevelAchievement) ? @[levelAchievement, scoreAchievement] : @[scoreAchievement];
    
    [GKAchievement reportAchievements:achievements withCompletionHandler:^(NSError *error) {
    }];
}

-(void)reportScore
{
    int level_sent = [_level.text intValue];
    
    GKScore *score = [[GKScore alloc] initWithLeaderboardIdentifier:@"SUMUPS_LEADERBOARDS"];
    score.value = level_sent;
    
    [GKScore reportScores:@[score] withCompletionHandler:^(NSError *error) {
        if (error != nil) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }];
}

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        // Annuler
    }
    
    else
    {
        // Ne sert strictement à rien
    }
}

- (IBAction)reagir:(id)sender
{
    [self audio_tools:@"clique"];
    
    int temporaire;
    int time_temp;
    
    time_temp= [_time.text intValue];
    temporaire=[_actuel.text intValue];
    
    if (time_temp>0 && win==false)
    {
    
    if (statut_button1==0) // PUSH
    {
        if (Mes_combinaisons<Nb_combinaison_max)
        {
        temporaire=temporaire+Button1;
        statut_button1=1;
    UIColor *colourBlue = [[UIColor alloc]initWithRed:63.0/255.0 green:75.0/255.0 blue:110.0/255.0 alpha:1.0];
        [_Label__Bouton1 setTintColor : colourBlue];
        [_Label__Bouton1 setBackgroundImage:[UIImage imageNamed:@"posfocused.png"] forState:UIControlStateNormal];
        Mes_combinaisons=Mes_combinaisons+1;
        }
        else
        {
            NSLog(@"Action impossible : Problème de combinaison");
        }
    }
    else // NO
    {
        temporaire=temporaire-Button1;
        statut_button1=0;
        UIColor *colourButton = [[UIColor alloc]initWithRed:0.0/255.0 green:51.0/255.0 blue:102.0/255.0 alpha:1.0];
        [_Label__Bouton1 setTintColor: colourButton];
        [_Label__Bouton1 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
        
        Mes_combinaisons=Mes_combinaisons-1;
    }
    
    NSString *variable = [@(temporaire) stringValue];
    _actuel.text = variable;
        
        if (temporaire==Objectif)
        {
            lvl = lvl+1;
            _level.text = [@(lvl) stringValue];
            
            [timer invalidate];
            
            NSLog(@"J'ai gagné");
            win=true;
            
            /*
             
             NSString *msg = [NSString stringWithFormat:@"You win ! Level up. "];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Well done !!" message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
             [alert addButtonWithTitle:@"Next level"];
             
             [alert show]; // */
            
            
            // -----------------------
            // DEBUG DEBUG DEBUG DEBUG
            
            [self check_score];
            
            [self audio_tools:@"win"];
            
            View_WIN *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"View_WIN"];
            
            [self presentViewController:second_view animated:NO completion:nil];
            
            [second_view setMy_Score:lvl]; // */
            [second_view setMy_Time:[_time.text intValue]];
        }
        
    }
    }

- (IBAction)reagir2:(id)sender
{
    [self audio_tools:@"clique"];
    
    int temporaire;
    int time_temp;
    
    time_temp= [_time.text intValue];
    temporaire=[_actuel.text intValue];
    
    if (time_temp>0 && win == false)
    {
    
    if (statut_button2==0) //PUSH
    {
        if (Mes_combinaisons<Nb_combinaison_max)
        {
        temporaire=temporaire+Button2;
        statut_button2=1;
            UIColor *colourBlue = [[UIColor alloc]initWithRed:63.0/255.0 green:75.0/255.0 blue:110.0/255.0 alpha:1.0];
            [_Label__Bouton2 setTintColor : colourBlue];
        [_Label__Bouton2 setBackgroundImage:[UIImage imageNamed:@"posfocused.png"] forState:UIControlStateNormal];
            Mes_combinaisons=Mes_combinaisons+1;
        }
        else
        {
            NSLog(@"Action impossible : Problème de combinaison");
        }
    }
    else // NO
    {
        temporaire=temporaire-Button2;
        statut_button2=0;
        UIColor *colourButton = [[UIColor alloc]initWithRed:0.0/255.0 green:51.0/255.0 blue:102.0/255.0 alpha:1.0];
        [_Label__Bouton2 setTintColor: colourButton];
        [_Label__Bouton2 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
        
        Mes_combinaisons=Mes_combinaisons-1;
    }
    
    NSString *variable = [@(temporaire) stringValue];
    
    _actuel.text = variable;
        
        if (temporaire==Objectif)
        {
            lvl = lvl+1;
            _level.text = [@(lvl) stringValue];
            
            [timer invalidate];
            
            NSLog(@"J'ai gagné");
            win=true;
            
            /*
             
             NSString *msg = [NSString stringWithFormat:@"You win ! Level up. "];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Well done !!" message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
             [alert addButtonWithTitle:@"Next level"];
             
             [alert show]; // */
            
            
            // -----------------------
            // DEBUG DEBUG DEBUG DEBUG
            
            [self check_score];
            
            [self audio_tools:@"win"];
            
            View_WIN *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"View_WIN"];
            
            [self presentViewController:second_view animated:NO completion:nil];
            
            [second_view setMy_Score:lvl]; // */
            [second_view setMy_Time:[_time.text intValue]];
        }
}
}

- (IBAction)reagir3:(id)sender
{
    [self audio_tools:@"clique"];
    
    int temporaire;
    int time_temp;
    
    time_temp= [_time.text intValue];
    temporaire=[_actuel.text intValue];
    
    if (time_temp>0 && win==false)
    {
    
    if (statut_button3==0) // PUSH
    {
        if (Mes_combinaisons<Nb_combinaison_max)
        {
        temporaire=temporaire+Button3;
        statut_button3=1;
            UIColor *colourBlue = [[UIColor alloc]initWithRed:63.0/255.0 green:75.0/255.0 blue:110.0/255.0 alpha:1.0];
            [_Label__Bouton3 setTintColor : colourBlue];
        [_Label__Bouton3 setBackgroundImage:[UIImage imageNamed:@"posfocused.png"] forState:UIControlStateNormal];
            Mes_combinaisons=Mes_combinaisons+1;
        }
        else
        {
            NSLog(@"Action impossible : Problème de combinaison");
        }
    }
    else // NO
    {
        temporaire=temporaire-Button3;
        statut_button3=0;
        UIColor *colourButton = [[UIColor alloc]initWithRed:0.0/255.0 green:51.0/255.0 blue:102.0/255.0 alpha:1.0];
        [_Label__Bouton3 setTintColor: colourButton];
        [_Label__Bouton3 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
        
        Mes_combinaisons=Mes_combinaisons-1;
    }
    
    NSString *variable = [@(temporaire) stringValue];
    
    _actuel.text = variable;
        
        if (temporaire==Objectif)
        {
            lvl = lvl+1;
            _level.text = [@(lvl) stringValue];
            
            [timer invalidate];
            
            NSLog(@"J'ai gagné");
            win=true;
            
            /*
             
             NSString *msg = [NSString stringWithFormat:@"You win ! Level up. "];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Well done !!" message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
             [alert addButtonWithTitle:@"Next level"];
             
             [alert show]; // */
            
            
            // -----------------------
            // DEBUG DEBUG DEBUG DEBUG
            
            [self check_score];
            
            [self audio_tools:@"win"];
            
            View_WIN *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"View_WIN"];
            
            [self presentViewController:second_view animated:NO completion:nil];
            
            [second_view setMy_Score:lvl]; // */
            [second_view setMy_Time:[_time.text intValue]];
        }
}
}

- (IBAction)reagir4:(id)sender
{
    [self audio_tools:@"clique"];
    
    int temporaire;
    int time_temp;
    
    time_temp= [_time.text intValue];
    temporaire=[_actuel.text intValue];
    
    if (time_temp>0 && win==false)
    {
    
    if (statut_button4==0) //PUSH
    {
        if (Mes_combinaisons<Nb_combinaison_max)
        {
        temporaire=temporaire+Button4;
        statut_button4=1;
            UIColor *colourBlue = [[UIColor alloc]initWithRed:63.0/255.0 green:75.0/255.0 blue:110.0/255.0 alpha:1.0];
            [_Label__Bouton4 setTintColor : colourBlue];
        [_Label__Bouton4 setBackgroundImage:[UIImage imageNamed:@"posfocused.png"] forState:UIControlStateNormal];
            Mes_combinaisons=Mes_combinaisons+1;
        }
        else
        {
            NSLog(@"Action impossible : Problème de combinaison");
        }
    }
    else // NO
    {
        temporaire=temporaire-Button4;
        statut_button4=0;
        UIColor *colourButton = [[UIColor alloc]initWithRed:0.0/255.0 green:51.0/255.0 blue:102.0/255.0 alpha:1.0];
        [_Label__Bouton4 setTintColor: colourButton];
        [_Label__Bouton4 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
        
        Mes_combinaisons=Mes_combinaisons-1;
    }
    
    NSString *variable = [@(temporaire) stringValue];
    
    _actuel.text = variable;
        
        if (temporaire==Objectif)
        {
            lvl = lvl+1;
            _level.text = [@(lvl) stringValue];
            
            [timer invalidate];
            
            NSLog(@"J'ai gagné");
            win=true;
            
            /*
             
             NSString *msg = [NSString stringWithFormat:@"You win ! Level up. "];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Well done !!" message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
             [alert addButtonWithTitle:@"Next level"];
             
             [alert show]; // */
            
            
            // -----------------------
            // DEBUG DEBUG DEBUG DEBUG
            
            [self check_score];
            
            [self audio_tools:@"win"];
            
            View_WIN *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"View_WIN"];
            
            [self presentViewController:second_view animated:NO completion:nil];
            
            [second_view setMy_Score:lvl]; // */
            [second_view setMy_Time:[_time.text intValue]];
        }
}
}

- (IBAction)reagir5:(id)sender
{
    [self audio_tools:@"clique"];
    
    int temporaire;
    int time_temp;
    
    time_temp= [_time.text intValue];
    temporaire=[_actuel.text intValue];
    
    if (time_temp>0 && win==false)
    {
    
    if (statut_button5==0) //PUSH
    {
        if (Mes_combinaisons<Nb_combinaison_max)
        {
        temporaire=temporaire+Button5;
        statut_button5=1;
            UIColor *colourBlue = [[UIColor alloc]initWithRed:63.0/255.0 green:75.0/255.0 blue:110.0/255.0 alpha:1.0];
            [_Label__Bouton5 setTintColor : colourBlue];
        [_Label__Bouton5 setBackgroundImage:[UIImage imageNamed:@"posfocused.png"] forState:UIControlStateNormal];
            Mes_combinaisons=Mes_combinaisons+1;
        }
        else
        {
            NSLog(@"Action impossible : Problème de combinaison");
        }
    }
    else // NO
    {
        temporaire=temporaire-Button5;
        statut_button5=0;
        UIColor *colourButton = [[UIColor alloc]initWithRed:0.0/255.0 green:51.0/255.0 blue:102.0/255.0 alpha:1.0];
        [_Label__Bouton5 setTintColor: colourButton];
        [_Label__Bouton5 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
        
        Mes_combinaisons=Mes_combinaisons-1;
    }
    
    NSString *variable = [@(temporaire) stringValue];
    
    _actuel.text = variable;
        
        if (temporaire==Objectif)
        {
            lvl = lvl+1;
            _level.text = [@(lvl) stringValue];
            
            [timer invalidate];
            
            NSLog(@"J'ai gagné");
            win=true;
            
            /*
             
             NSString *msg = [NSString stringWithFormat:@"You win ! Level up. "];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Well done !!" message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
             [alert addButtonWithTitle:@"Next level"];
             
             [alert show]; // */
            
            
            // -----------------------
            // DEBUG DEBUG DEBUG DEBUG
            
            [self check_score];
            
            [self audio_tools:@"win"];
            
            View_WIN *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"View_WIN"];
            
            [self presentViewController:second_view animated:NO completion:nil];
            
            [second_view setMy_Score:lvl]; // */
            [second_view setMy_Time:[_time.text intValue]];
        }
}
}

- (IBAction)reagir6:(id)sender
{
    [self audio_tools:@"clique"];
    
    int temporaire;
    int time_temp;
    
    time_temp= [_time.text intValue];
    temporaire=[_actuel.text intValue];
    
    if (time_temp>0 && win==false)
    {
    
    if (statut_button6==0) //PUSH
    {
        if (Mes_combinaisons<Nb_combinaison_max)
        {
        temporaire=temporaire+Button6;
        statut_button6=1;
            UIColor *colourBlue = [[UIColor alloc]initWithRed:63.0/255.0 green:75.0/255.0 blue:110.0/255.0 alpha:1.0];
            [_Label__Bouton6 setTintColor : colourBlue];
        [_Label__Bouton6 setBackgroundImage:[UIImage imageNamed:@"posfocused.png"] forState:UIControlStateNormal];
            Mes_combinaisons=Mes_combinaisons+1;
        }
        else
        {
            NSLog(@"Action impossible : Problème de combinaison");
        }
    }
    else // NO
    {
        temporaire=temporaire-Button6;
        statut_button6=0;
        UIColor *colourButton = [[UIColor alloc]initWithRed:0.0/255.0 green:51.0/255.0 blue:102.0/255.0 alpha:1.0];
        [_Label__Bouton6 setTintColor: colourButton];
        [_Label__Bouton6 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
        
        Mes_combinaisons=Mes_combinaisons-1;
    }
    
    NSString *variable = [@(temporaire) stringValue];
    
    _actuel.text = variable;
        
        if (temporaire==Objectif)
        {
            lvl = lvl+1;
            _level.text = [@(lvl) stringValue];
            
            [timer invalidate];
            
            NSLog(@"J'ai gagné");
            win=true;
            
            /*
             
             NSString *msg = [NSString stringWithFormat:@"You win ! Level up. "];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Well done !!" message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
             [alert addButtonWithTitle:@"Next level"];
             
             [alert show]; // */
            
            
            // -----------------------
            // DEBUG DEBUG DEBUG DEBUG
            
            [self check_score];
            
            [self audio_tools:@"win"];
            
            View_WIN *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"View_WIN"];
            
            [self presentViewController:second_view animated:NO completion:nil];
            
            [second_view setMy_Score:lvl]; // */
            [second_view setMy_Time:[_time.text intValue]];
        }
}
}

- (IBAction)reagir7:(id)sender
{
    [self audio_tools:@"clique"];
    
    int temporaire;
    int time_temp;
    
    time_temp= [_time.text intValue];
    temporaire=[_actuel.text intValue];
    
    if (time_temp>0 && win==false)
    {
    
    if (statut_button7==0) //PUSH
    {
        if (Mes_combinaisons<Nb_combinaison_max)
        {
        temporaire=temporaire+Button7;
        statut_button7=1;
            UIColor *colourBlue = [[UIColor alloc]initWithRed:63.0/255.0 green:75.0/255.0 blue:110.0/255.0 alpha:1.0];
            [_Label__Bouton7 setTintColor : colourBlue];
        [_Label__Bouton7 setBackgroundImage:[UIImage imageNamed:@"posfocused.png"] forState:UIControlStateNormal];
            Mes_combinaisons=Mes_combinaisons+1;
        }
        else
        {
            NSLog(@"Action impossible : Problème de combinaison");
        }
    }
    else // NO
    {
        temporaire=temporaire-Button7;
        statut_button7=0;
        UIColor *colourButton = [[UIColor alloc]initWithRed:0.0/255.0 green:51.0/255.0 blue:102.0/255.0 alpha:1.0];
        [_Label__Bouton7 setTintColor: colourButton];
        [_Label__Bouton7 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
        
        Mes_combinaisons=Mes_combinaisons-1;
    }
    
    NSString *variable = [@(temporaire) stringValue];
    
    _actuel.text = variable;
        
        if (temporaire==Objectif)
        {
            lvl = lvl+1;
            _level.text = [@(lvl) stringValue];
            
            [timer invalidate];
            
            NSLog(@"J'ai gagné");
            win=true;
            
            /*
             
             NSString *msg = [NSString stringWithFormat:@"You win ! Level up. "];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Well done !!" message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
             [alert addButtonWithTitle:@"Next level"];
             
             [alert show]; // */
            
            
            // -----------------------
            // DEBUG DEBUG DEBUG DEBUG
            
            [self check_score];
            
            [self audio_tools:@"win"];
            
            View_WIN *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"View_WIN"];
            
            [self presentViewController:second_view animated:NO completion:nil];
            
            [second_view setMy_Score:lvl]; // */
            [second_view setMy_Time:[_time.text intValue]];
        }
}
}

- (IBAction)reagir8:(id)sender
{
    [self audio_tools:@"clique"];
    
    int temporaire;
    int time_temp;
    
    time_temp= [_time.text intValue];
    temporaire=[_actuel.text intValue];
    
    if (time_temp>0 && win==false)
    {
    
    if (statut_button8==0) //PUSH
    {
        if (Mes_combinaisons<Nb_combinaison_max)
        {
        temporaire=temporaire+Button8;
        statut_button8=1;
            UIColor *colourBlue = [[UIColor alloc]initWithRed:63.0/255.0 green:75.0/255.0 blue:110.0/255.0 alpha:1.0];
            [_Label__Bouton8 setTintColor : colourBlue];
        [_Label__Bouton8 setBackgroundImage:[UIImage imageNamed:@"posfocused.png"] forState:UIControlStateNormal];
            Mes_combinaisons=Mes_combinaisons+1;
        }
        else
        {
            NSLog(@"Action impossible : Problème de combinaison");
        }
    }
    else // NO
    {
        temporaire=temporaire-Button8;
        statut_button8=0;
        UIColor *colourButton = [[UIColor alloc]initWithRed:0.0/255.0 green:51.0/255.0 blue:102.0/255.0 alpha:1.0];
        [_Label__Bouton8 setTintColor: colourButton];
        [_Label__Bouton8 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
        Mes_combinaisons=Mes_combinaisons-1;
    }
    
    NSString *variable = [@(temporaire) stringValue];
    
    _actuel.text = variable;
        
        if (temporaire==Objectif)
        {
            lvl = lvl+1;
            _level.text = [@(lvl) stringValue];
            
            [timer invalidate];
            
            NSLog(@"J'ai gagné");
            win=true;
            
            /*
             
             NSString *msg = [NSString stringWithFormat:@"You win ! Level up. "];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Well done !!" message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
             [alert addButtonWithTitle:@"Next level"];
             
             [alert show]; // */
            
            
            // -----------------------
            // DEBUG DEBUG DEBUG DEBUG
            
            [self check_score];
            
            [self audio_tools:@"win"];
            
            View_WIN *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"View_WIN"];
            
            [self presentViewController:second_view animated:NO completion:nil];
            
            [second_view setMy_Score:lvl]; // */
            [second_view setMy_Time:[_time.text intValue]];
        }
}
}

- (IBAction)reagir9:(id)sender
{
    [self audio_tools:@"clique"];
    
    int temporaire;
    int time_temp;
    
    time_temp= [_time.text intValue];
    temporaire=[_actuel.text intValue];
    
    if (time_temp>0 && win==false)
    {
    
    if (statut_button9==0) //PUSH
    {
        if (Mes_combinaisons<Nb_combinaison_max)
        {
        temporaire=temporaire+Button9;
        statut_button9=1;
            UIColor *colourBlue = [[UIColor alloc]initWithRed:63.0/255.0 green:75.0/255.0 blue:110.0/255.0 alpha:1.0];
            [_Label__Bouton9 setTintColor : colourBlue];
        [_Label__Bouton9 setBackgroundImage:[UIImage imageNamed:@"posfocused.png"] forState:UIControlStateNormal];
            Mes_combinaisons=Mes_combinaisons+1;
        }
        else
        {
            NSLog(@"Action impossible : Problème de combinaison");
        }
    }
    else // NO
    {
        temporaire=temporaire-Button9;
        statut_button9=0;
        
         UIColor *colourButton = [[UIColor alloc]initWithRed:0.0/255.0 green:51.0/255.0 blue:102.0/255.0 alpha:1.0];
       [_Label__Bouton9 setTintColor: colourButton];
        
        [_Label__Bouton9 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
        Mes_combinaisons=Mes_combinaisons-1;
    }
    
    NSString *variable = [@(temporaire) stringValue];
    
    _actuel.text = variable;
    
    
    if (temporaire==Objectif)
    {
        lvl = lvl+1;
        _level.text = [@(lvl) stringValue];
        
        [timer invalidate];
        
        NSLog(@"J'ai gagné");
        win=true;
        
        /*
        
        NSString *msg = [NSString stringWithFormat:@"You win ! Level up. "];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Well done !!" message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
        [alert addButtonWithTitle:@"Next level"];
        
        [alert show]; // */
        
        
        // -----------------------
        // DEBUG DEBUG DEBUG DEBUG
        
        [self check_score];
        
        [self audio_tools:@"win"];
        
        View_WIN *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"View_WIN"];
        
        [self presentViewController:second_view animated:NO completion:nil];
        
        [second_view setMy_Score:lvl]; // */
        [second_view setMy_Time:[_time.text intValue]];
    }   
}
}

@end
