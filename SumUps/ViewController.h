//
//  ViewController.h
//  sumUps
//
//  Created by Audouin d'Aboville on 12/07/2014.
//  Copyright (c) 2014 Audouin d'Aboville. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioServices.h>

#import "View_WIN.h"
#import "View_LOOSE.h"
#import "Help.h"
#import "Score.h"
#import "stdlib.h"
#import "math.h"
#import "AppDelegate.h"

#include <stdlib.h>
#include <stdio.h>

@interface ViewController : UIViewController
{
    //1=push button 0=no
    
    UIWebView *Pub;
    
    int Button1;
    bool statut_button1;
    
    NSTimer *timer;
    
    int Button2;
    bool statut_button2;
    
    int Button3;
    bool statut_button3;
    
    int Button4;
    bool statut_button4;
    
    int Button5;
    bool statut_button5;
    
    int Button6;
    bool statut_button6;
    
    int Button7;
    bool statut_button7;
    
    int Button8;
    bool statut_button8;
    
    int Button9;
    bool statut_button9;
    
    int Nb_combinaison_max;
    int Mes_combinaisons;
    
    int var_zero_test;
    
    int lvl;
    int lvl_max;
    int time;
    bool win;
    int Objectif;
    int Actuel;
    
    int low_bound;
    int high_bound;
    
    int transfert_solution[6];
}


@property (weak, nonatomic) IBOutlet UIWebView *Pub_Game;

@property (weak, nonatomic) IBOutlet UIButton *Label__Bouton1;
@property (weak, nonatomic) IBOutlet UIButton *Label__Bouton2;
@property (weak, nonatomic) IBOutlet UIButton *Label__Bouton3;
@property (weak, nonatomic) IBOutlet UIButton *Label__Bouton4;
@property (weak, nonatomic) IBOutlet UIButton *Label__Bouton5;
@property (weak, nonatomic) IBOutlet UIButton *Label__Bouton6;
@property (weak, nonatomic) IBOutlet UIButton *Label__Bouton7;
@property (weak, nonatomic) IBOutlet UIButton *Label__Bouton8;
@property (weak, nonatomic) IBOutlet UIButton *Label__Bouton9;

@property (weak, nonatomic) IBOutlet UILabel *actuel;
@property (weak, nonatomic) IBOutlet UILabel *goal;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *level;

- (void) JaiPerdu;

- (void)audio_tools:(NSString*)which_one;

- (void) check_score;

-(void)reportScore;

-(void)updateAchievements;

- (void) Set_lvl:(int)level;

- (IBAction)reagir:(id)sender;

- (IBAction)reagir2:(id)sender;

- (IBAction)reagir3:(id)sender;

- (IBAction)reagir4:(id)sender;

- (IBAction)reagir5:(id)sender;

- (IBAction)reagir6:(id)sender;

- (IBAction)reagir7:(id)sender;

- (IBAction)reagir8:(id)sender;

- (IBAction)reagir9:(id)sender;

@end
