//
//  Solution.m
//  SumUps
//
//  Created by Audouin d'Aboville on 21/10/2014.
//  Copyright (c) 2014 Audouin d'Aboville. All rights reserved.
//

#import "Solution.h"

@interface Solution ()

@end

@implementation Solution

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"init solution");
    
    //  Espace publicitaire & google analytics
    
    [_Pub loadRequest:[NSURLRequest requestWithURL: [NSURL URLWithString : @"http://www.ad-inc.fr/Autres/SumUp/solution.html"]]];
    
    NSLog(@"Analytics init ok");
    
    cpt=0;
    cpt2=0;
    
    // Colorisation de l'affichage
    
    UIColor *colourButton = [[UIColor alloc]initWithRed:0.0/255.0 green:51.0/255.0 blue:102.0/255.0 alpha:1.0];
    
    [_Button1 setTintColor: colourButton];
    [_Button2 setTintColor: colourButton];
    [_Button3 setTintColor: colourButton];
    [_Button4 setTintColor: colourButton];
    [_Button5 setTintColor: colourButton];
    [_Button6 setTintColor: colourButton];
    [_Button7 setTintColor: colourButton];
    [_Button8 setTintColor: colourButton];
    [_Button9 setTintColor: colourButton];
    
    // -----------------------------------------------
    
    // Initialisation du timer d'évolution.
    
    UIBackgroundTaskIdentifier bgTask = UIBackgroundTaskInvalid;
    UIApplication *app = [UIApplication sharedApplication];
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
    }];
    
    evolution = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self   selector:@selector(body) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:evolution forMode:NSRunLoopCommonModes];
    
    // Vérification des "Achivement"
    [self updateAchievements];
}

-(void)updateAchievements
{
    NSLog(@"updateAchivement");
    
    NSString *achievementIdentifier;
    float progressPercentage = 0.0;
    BOOL progressInLevelAchievement = NO;
    
    GKAchievement *levelAchievement = nil;
    GKAchievement *scoreAchievement = nil;
    
    // Vérification préalable
    
    NSString *contenu; // Contient le contenu du fichier "first_Help.txt"
    NSString *chemineau = [[self applicationDocumentsDirectory].path
                           stringByAppendingPathComponent:@"first_solution.txt"];
    contenu = [NSString stringWithContentsOfFile:chemineau encoding:NSUTF8StringEncoding error:nil];
    
    
    if ([contenu isEqual:@"0"] || contenu == NULL)
    {
        NSLog(@"Solution achivement ON");
        [GKNotificationBanner showBannerWithTitle:@"Solution !" message:@"You know how to get the solution !" completionHandler:nil];
        progressPercentage = 100;
        achievementIdentifier = @"SUMUPS_Achivement_Solution";
        progressInLevelAchievement = YES;
        
        // Création du fichier first_Help
        NSString *path = [[self applicationDocumentsDirectory].path
                          stringByAppendingPathComponent:@"first_solution.txt"];
        bool rendu = [@"1" writeToFile:path atomically:YES
                              encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"Création du fichier first solution : %d", rendu);
    }
    
    if (progressInLevelAchievement)
    {
        levelAchievement = [[GKAchievement alloc] initWithIdentifier:achievementIdentifier];
        levelAchievement.percentComplete = progressPercentage;
    }
    
    scoreAchievement = [[GKAchievement alloc] initWithIdentifier:achievementIdentifier];
    scoreAchievement.percentComplete = progressPercentage;
    
    NSArray *achievements = (progressInLevelAchievement) ? @[levelAchievement, scoreAchievement] : @[scoreAchievement];
    
    [GKAchievement reportAchievements:achievements withCompletionHandler:^(NSError *error) {
    }];
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)body 
{
    NSLog(@"----------------");
    NSLog(@"body in progress");
    
    NSString *bouton; // Contenu du bouton [][][][]...
    
    UIColor *colourBlue = [UIColor whiteColor]; // Initialisation des couleurs
    NSString *background = @"negbut.png"; // Initialisation du background
    
    NSLog(@"cpt : %d", cpt);
    NSLog(@"je recherche: %d", my_transfert_obj[cpt]);
    NSLog(@" - - - ");
    
    if (my_transfert_obj[cpt]!=0) // On s'occupe que de ce qu'il nous interresse dans le tableau
    {
        bouton=[_Button1 currentTitle];
        int var_temp = [bouton intValue];
        
        if (var_temp==my_transfert_obj[cpt] && _Button1.currentTitleColor != [UIColor whiteColor])
        {
            cpt=cpt+1;
            
            [_Button1 setTintColor: colourBlue];
            [_Button1 setBackgroundImage:[UIImage imageNamed:background] forState:UIControlStateNormal];
            
            NSLog(@"check button1: %d", cpt);
            return;
        }
        
        bouton=[_Button2 currentTitle];
        var_temp = [bouton intValue];
        if (var_temp==my_transfert_obj[cpt] && _Button2.currentTitleColor != [UIColor whiteColor])
        {
            cpt=cpt+1;
            
            [_Button2 setTintColor: colourBlue];
            [_Button2 setBackgroundImage:[UIImage imageNamed:background] forState:UIControlStateNormal];
            
            NSLog(@"check button2: %d", cpt);
            return;
        }
        
        bouton=[_Button3 currentTitle];
        var_temp = [bouton intValue];
        if (var_temp==my_transfert_obj[cpt] && _Button3.currentTitleColor != [UIColor whiteColor])
        {
            cpt=cpt+1;
            
            [_Button3 setTintColor: colourBlue];
            [_Button3 setBackgroundImage:[UIImage imageNamed:background] forState:UIControlStateNormal];
            
            NSLog(@"check button3: %d", cpt);
            return;
        }
        
        bouton=[_Button4 currentTitle];
        var_temp = [bouton intValue];
        if (var_temp==my_transfert_obj[cpt] && _Button4.currentTitleColor != [UIColor whiteColor])
        {
            cpt=cpt+1;
            
            [_Button4 setTintColor: colourBlue];
            [_Button4 setBackgroundImage:[UIImage imageNamed:background] forState:UIControlStateNormal];
            
            NSLog(@"check button4: %d", cpt);
            return;
        }
        
        bouton=[_Button5 currentTitle];
        var_temp = [bouton intValue];
        if (var_temp==my_transfert_obj[cpt] && _Button5.currentTitleColor != [UIColor whiteColor])
        {
            cpt=cpt+1;
            
            [_Button5 setTintColor: colourBlue];
            [_Button5 setBackgroundImage:[UIImage imageNamed:background] forState:UIControlStateNormal];
            
            NSLog(@"check button5: %d", cpt);
            return;
        }
        
        bouton=[_Button6 currentTitle];
        var_temp = [bouton intValue];
        if (var_temp==my_transfert_obj[cpt] && _Button6.currentTitleColor != [UIColor whiteColor])
        {
            cpt=cpt+1;
            
            [_Button6 setTintColor: colourBlue];
            [_Button6 setBackgroundImage:[UIImage imageNamed:background] forState:UIControlStateNormal];
            
            NSLog(@"check button6: %d", cpt);
            return;
        }
        
        bouton=[_Button7 currentTitle];
        var_temp = [bouton intValue];
        if (var_temp==my_transfert_obj[cpt] && _Button7.currentTitleColor != [UIColor whiteColor])
        {
            cpt=cpt+1;
            
            [_Button7 setTintColor: colourBlue];
            [_Button7 setBackgroundImage:[UIImage imageNamed:background] forState:UIControlStateNormal];
            
            NSLog(@"check button7: %d", cpt);
            return;
        }
        
        bouton=[_Button8 currentTitle];
        var_temp = [bouton intValue];
        if (var_temp==my_transfert_obj[cpt] && _Button9.currentTitleColor != [UIColor whiteColor])
        {
            cpt=cpt+1;
            
            [_Button8 setTintColor: colourBlue];
            [_Button8 setBackgroundImage:[UIImage imageNamed:background] forState:UIControlStateNormal];
            
            NSLog(@"check button8: %d", cpt);
            return;
        }
        
        bouton=[_Button9 currentTitle];
        var_temp = [bouton intValue];
        if (var_temp==my_transfert_obj[cpt] && _Button9.currentTitleColor != [UIColor whiteColor])
        {
            cpt=cpt+1;
            
            [_Button9 setTintColor: colourBlue];
            [_Button9 setBackgroundImage:[UIImage imageNamed:background] forState:UIControlStateNormal];
            
            NSLog(@"check button9 : %d", cpt);
            return;
        }
    }
    
    else
    {
        cpt2=cpt2+1;
        NSLog(@"Done ! Solution OK");
        
        if (cpt2==3) // Timer d'attente avant transfert vers page view_loose_pay
        {
            [evolution invalidate];
            NSLog(@"OVER!");
            
            View_LOOSE_pay *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"View_LOOSE_pay"];
            
            [self presentViewController:second_view animated:NO completion:nil];
            
            [second_view setMy_Score:level];
        }
    }
}

- (void)setMy_Score: (int)lvl
{
    level = lvl;
    
    NSString *variable = [@(level) stringValue];
    _my_level.text=variable;
    
    UIColor *colour1 = [[UIColor alloc]initWithRed:254.0/255.0 green:233.0/255.0 blue:218.0/255.0 alpha:1.0];
    UIColor *colour2 = [[UIColor alloc]initWithRed:221.0/255.0 green:240.0/255.0 blue:175.0/255.0 alpha:1.0];
    UIColor *colour3 = [[UIColor alloc]initWithRed:189.0/255.0 green:243.0/255.0 blue:231.0/255.0 alpha:1.0];
    UIColor *colour4 = [[UIColor alloc]initWithRed:233.0/255.0 green:204.0/255.0 blue:242.0/255.0 alpha:1.0];
    UIColor *colour5 = [[UIColor alloc]initWithRed:227.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1.0];

    
    if (level < 5)
    {
        self.view.backgroundColor = colour1;
    }
    
    else if (level>=5 && level <10)
    {
        self.view.backgroundColor = colour2; // Changement background
    }
    
    else if (level >= 10 && level <= 15)
    {
        self.view.backgroundColor = colour3; // Changement background
    }
    
    else
    {
        self.view.backgroundColor = colour4; // Changement background
    }
    
    if (level >=20)
    {
        self.view.backgroundColor = colour5;
    }
}

- (void)setMy_Goal:(int)goal
{
    my_goal = goal;
    
    NSString *variable = [@(my_goal) stringValue];
    _goal.text=variable;
}

- (void)setMy_Board:(int)myButton1 andNum2:(int)myButton2 andNum3:(int)myButton3 andNum4:(int)myButton4 andNum5:(int)myButton5 andNum6:(int)myButton6 andNum7:(int)myButton7 andNum8:(int)myButton8 andNum9:(int)myButton9
{
    my_button1 = myButton1;
    my_button2 = myButton2;
    my_button3 = myButton3;
    
    my_button4 = myButton4;
    my_button5 = myButton5;
    my_button6 = myButton6;
    
    my_button7 = myButton7;
    my_button8 = myButton8;
    my_button9 = myButton9;
    
    NSString *variable = [@(my_button1) stringValue];
    [_Button1 setTitle:variable forState:UIControlStateNormal];
    
    variable = [@(my_button2) stringValue];
    [_Button2 setTitle:variable forState:UIControlStateNormal];
    
    variable = [@(my_button3) stringValue];
    [_Button3 setTitle:variable forState:UIControlStateNormal];
    
    variable = [@(my_button4) stringValue];
    [_Button4 setTitle:variable forState:UIControlStateNormal];
    
    variable = [@(my_button5) stringValue];
    [_Button5 setTitle:variable forState:UIControlStateNormal];
    
    variable = [@(my_button6) stringValue];
    [_Button6 setTitle:variable forState:UIControlStateNormal];
    
    variable = [@(my_button7) stringValue];
    [_Button7 setTitle:variable forState:UIControlStateNormal];
    
    variable = [@(my_button8) stringValue];
    [_Button8 setTitle:variable forState:UIControlStateNormal];
    
    variable = [@(my_button9) stringValue];
    [_Button9 setTitle:variable forState:UIControlStateNormal];
}

- (void)setMy_Solution:(int *)transfert_solution
{
    my_transfert_obj[0] = transfert_solution[0];
    my_transfert_obj[1] = transfert_solution[1];
    my_transfert_obj[2] = transfert_solution[2];
    my_transfert_obj[3] = transfert_solution[3];
    my_transfert_obj[4] = transfert_solution[4];
    my_transfert_obj[5] = transfert_solution[5];
}

@end