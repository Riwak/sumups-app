//
//  View_LOOSE_pay.m
//  SumUps
//
//  Created by Audouin d'Aboville on 22/10/2014.
//  Copyright (c) 2014 Audouin d'Aboville. All rights reserved.
//

#import "View_LOOSE_pay.h"

@interface View_LOOSE_pay ()

@end

@implementation View_LOOSE_pay

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"Ouverture de la fenêtre 'LOOSE' ! ");
    
    UIColor *colour1 = [[UIColor alloc]initWithRed:254.0/255.0 green:233.0/255.0 blue:218.0/255.0 alpha:1.0];
    UIColor *colour2 = [[UIColor alloc]initWithRed:221.0/255.0 green:240.0/255.0 blue:175.0/255.0 alpha:1.0];
    UIColor *colour3 = [[UIColor alloc]initWithRed:189.0/255.0 green:243.0/255.0 blue:231.0/255.0 alpha:1.0];
    UIColor *colour4 = [[UIColor alloc]initWithRed:233.0/255.0 green:204.0/255.0 blue:242.0/255.0 alpha:1.0];
    UIColor *colour5 = [[UIColor alloc]initWithRed:227.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1.0];
    
    int lvl_background_reserved_variable = [_display_lvl.text intValue];
    
    if (lvl_background_reserved_variable < 5)
    {
        self.view.backgroundColor = colour1;
    }
    
    else if (lvl_background_reserved_variable>=5 && lvl_background_reserved_variable <10)
    {
        self.view.backgroundColor = colour2; // Changement background
    }
    
    else if (lvl_background_reserved_variable >= 10 && lvl_background_reserved_variable <= 15)
    {
        self.view.backgroundColor = colour3; // Changement background
    }
    
    else
    {
        self.view.backgroundColor = colour4; // Changement background
    }
    
    if (lvl_background_reserved_variable >= 20)
    {
        self.view.backgroundColor = colour5;
    }
    
    // ---------------------------------------
    // Lecture du fichier de sauvegarde Life
    
    NSString *chemin = [[self applicationDocumentsDirectory].path
                        stringByAppendingPathComponent:@"Life.txt"];
    NSString *Var_temp1 = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
    
    NSLog(@"Resultat life: %@", Var_temp1);
    
    if (Var_temp1==NULL)
    {
        NSLog(@"Problème de vérification du fichier vie");
    }
    
    life = [Var_temp1 intValue]; // La j'ai mon nombre de vie récupéré
    
    if (life == 0)
    {
        _life1.image = nil;
        _life2.image = nil;
        _life3.image = nil;
    }
    else if (life == 1)
    {
        _no_life1.image = nil;
        _life2.image = nil;
        _life3.image = nil;
    }
    else if (life == 2)
    {
        _no_life1.image = nil;
        _no_life2.image = nil;
        _life3.image = nil;
    }
    else if (life == 3)
    {
        _no_life1.image = nil;
        _no_life2.image = nil;
        _no_life3.image = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setMy_Score: (int)lvl
{
    NSString *variable = [@(lvl) stringValue];
    _display_lvl.text = variable;
    
    level = lvl;
    
    UIColor *colour1 = [[UIColor alloc]initWithRed:254.0/255.0 green:233.0/255.0 blue:218.0/255.0 alpha:1.0];
    UIColor *colour2 = [[UIColor alloc]initWithRed:221.0/255.0 green:240.0/255.0 blue:175.0/255.0 alpha:1.0];
    UIColor *colour3 = [[UIColor alloc]initWithRed:189.0/255.0 green:243.0/255.0 blue:231.0/255.0 alpha:1.0];
    UIColor *colour4 = [[UIColor alloc]initWithRed:233.0/255.0 green:204.0/255.0 blue:242.0/255.0 alpha:1.0];
    UIColor *colour5 = [[UIColor alloc]initWithRed:227.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1.0];

    
    if (level < 5)
    {
        self.view.backgroundColor = colour1;
    }
    
    else if (level>=5 && level <10)
    {
        self.view.backgroundColor = colour2;
    }
    
    else if (level >= 10 && level <= 15)
    {
        self.view.backgroundColor = colour3;
    }
    
    else
    {
        self.view.backgroundColor = colour4;
    }
    
    if (level >= 20)
    {
        self.view.backgroundColor = colour5;
    }
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

-(void)updateAchievements:(NSString*)wich_button
{
    NSLog(@"updateAchivement");
    
    NSString *achievementIdentifier;
    float progressPercentage = 0.0;
    BOOL progressInLevelAchievement = NO;
    
    GKAchievement *levelAchievement = nil;
    GKAchievement *scoreAchievement = nil;
    
    // Vérification préalable
    
    NSString *contenu; // Contient le contenu du fichier "first_Twitter.txt"
    NSString *chemineau = [[self applicationDocumentsDirectory].path
                           stringByAppendingPathComponent:@"first_Twitter.txt"];
    contenu = [NSString stringWithContentsOfFile:chemineau encoding:NSUTF8StringEncoding error:nil];
    
    NSString *contenu2; // Contient le contenu du fichier "first_Facebook.txt"
    NSString *chemineau2 = [[self applicationDocumentsDirectory].path
                            stringByAppendingPathComponent:@"first_Facebook.txt"];
    contenu2 = [NSString stringWithContentsOfFile:chemineau2 encoding:NSUTF8StringEncoding error:nil];
    
    if ([wich_button  isEqual: @"Twitter"])
    {
        if ([contenu isEqual:@"0"] || contenu == NULL)
        {
            NSLog(@"Twitter achivement ON");
            [GKNotificationBanner showBannerWithTitle:@"Score on Twitter !" message:@"Thanx for sharing, you earn 25 points" completionHandler:nil];
            progressPercentage = 100;
            achievementIdentifier = @"SUMUPS_Achivement_Twitter";
            progressInLevelAchievement = YES;
            
            // Création du fichier first_twitter
            NSString *path = [[self applicationDocumentsDirectory].path
                              stringByAppendingPathComponent:@"first_Twitter.txt"];
            bool rendu = [@"1" writeToFile:path atomically:YES
                                  encoding:NSUTF8StringEncoding error:nil];
            NSLog(@"Création du fichier first Twitter : %d", rendu);
        }
    }
    
    else if ([wich_button  isEqual: @"Facebook"])
    {
        if ([contenu2 isEqual:@"0"] || contenu2 == NULL)
        {
            NSLog(@"Facebook achivement ON");
            [GKNotificationBanner showBannerWithTitle:@"Score on Facebook !" message:@"Thanx for sharing, you earn 50 points" completionHandler:nil];
            progressPercentage = 100;
            achievementIdentifier = @"SUMUPS_Achivement_Facebook";
            progressInLevelAchievement = YES;
            
            // Création du fichier first_facebook
            NSString *path = [[self applicationDocumentsDirectory].path
                              stringByAppendingPathComponent:@"first_Facebook.txt"];
            bool rendu = [@"1" writeToFile:path atomically:YES
                                  encoding:NSUTF8StringEncoding error:nil];
            NSLog(@"Création du fichier first Facebook : %d", rendu);
        }
    }
    
    if (progressInLevelAchievement)
    {
        levelAchievement = [[GKAchievement alloc] initWithIdentifier:achievementIdentifier];
        levelAchievement.percentComplete = progressPercentage;
    }
    
    scoreAchievement = [[GKAchievement alloc] initWithIdentifier:achievementIdentifier];
    scoreAchievement.percentComplete = progressPercentage;
    
    NSArray *achievements = (progressInLevelAchievement) ? @[levelAchievement, scoreAchievement] : @[scoreAchievement];
    
    [GKAchievement reportAchievements:achievements withCompletionHandler:^(NSError *error) {
    }];
}

- (IBAction)twitter_share:(id)sender
{
    NSLog(@"Twitter");
    
    // Vérification des "Achivement"
    [self updateAchievements:@"Twitter"];
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheetOBJ = [SLComposeViewController
                                                  composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        NSString  *Var_temp1 = _display_lvl.text;
        
        NSString *msg = [NSString stringWithFormat:@"I reach the level %@ on SumUp ! Join me ! http://ad-inc.pagesperso-orange.fr/SumUp/", Var_temp1];
        
        [tweetSheetOBJ setInitialText:msg];
        [self presentViewController:tweetSheetOBJ animated:YES completion:nil];
    }
}

- (IBAction)Facebook_share:(id)sender
{
    NSLog(@"Facebook");
    
    // Vérification des "Achivement"
    [self updateAchievements:@"Facebook"];
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController * fbSheetOBJ = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        NSString  *Var_temp1 = _display_lvl.text;
        
        
        NSString *msg = [NSString stringWithFormat:@"I reach the level %@ on SumUp ! Join me !", Var_temp1];
        
        [fbSheetOBJ setInitialText:msg];
        [fbSheetOBJ addURL:[NSURL URLWithString:@"http://ad-inc.pagesperso-orange.fr/SumUp/"]];
        [fbSheetOBJ addImage:[UIImage imageNamed:@"logoSumUp.png"]];
        
        [self presentViewController:fbSheetOBJ animated:YES completion:Nil];
    }
}

- (IBAction)restart:(id)sender
{
    NSLog(@"Passage vers ViewController");
    
    ViewController *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    
    [self presentViewController:second_view animated:NO completion:nil];
}

- (IBAction)cancel:(id)sender
{
    NSLog(@"Passage vers menu");
    
    Menu *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    
    [self presentViewController:second_view animated:NO completion:nil];
}

@end