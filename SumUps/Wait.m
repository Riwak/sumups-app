//
//  Wait.m
//  SumUps
//
//  Created by Audouin d'Aboville on 13/03/2016.
//  Copyright © 2016 Audouin d'Aboville. All rights reserved.
//

#import "Wait.h"

@interface Wait ()

@end

@implementation Wait

- (void)viewDidLoad {
    [super viewDidLoad];
   
}

NSString* machineName()
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}

- (void)viewDidAppear:(BOOL)animated
{
    if([machineName() isEqualToString:@"iPhone7,2"])
    {
        NSLog(@"iPhone6");
        
        // Storyboard spécialisé actif : OK!
        
        NSLog(@"Changement automatique de storyboard");
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"iPhone6" bundle:nil];
        UIViewController* initialHelpView = [storyboard instantiateViewControllerWithIdentifier:@"Menu"];
        initialHelpView.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:initialHelpView animated:YES];
    }
    
    else if([machineName() isEqualToString:@"iPhone7,1"])
    {
        NSLog(@"iPhone6+");
        
        // Storyboard spécialisé actif : OK!
        
        NSLog(@"Changement automatique de storyboard");
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"iPhone6" bundle:nil];
        UIViewController* initialHelpView = [storyboard instantiateViewControllerWithIdentifier:@"Menu"];
        initialHelpView.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:initialHelpView animated:YES];
    }
    
    else if([machineName() isEqualToString:@"iPhone8,1"])
    {
        NSLog(@"iPhone6S");
        
        // Storyboard spécialisé actif : OK!
        
        NSLog(@"Changement automatique de storyboard");
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"iPhone6" bundle:nil];
        UIViewController* initialHelpView = [storyboard instantiateViewControllerWithIdentifier:@"Menu"];
        initialHelpView.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:initialHelpView animated:YES];
    }
    
    else if([machineName() isEqualToString:@"iPhone8,2"])
    {
        NSLog(@"iPhone6S+");
        
        // Storyboard spécialisé actif : OK!
        NSLog(@"Changement automatique de storyboard");
        
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"iPhone6" bundle:nil];
        UIViewController* initialHelpView = [storyboard instantiateViewControllerWithIdentifier:@"Menu"];
        initialHelpView.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:initialHelpView animated:YES];
    }
    
    else
    {
        NSLog(@"Autre iphone");
        
        // Storyboard spécialisé actif : OK!
        NSLog(@"Changement automatique de storyboard");
        
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* initialHelpView = [storyboard instantiateViewControllerWithIdentifier:@"Menu"];
        initialHelpView.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:initialHelpView animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
