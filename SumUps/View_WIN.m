//
//  View_WIN.m
//  sumUps
//
//  Created by Audouin d'Aboville on 12/07/2014.
//  Copyright (c) 2014 Audouin d'Aboville. All rights reserved.
//

#import "View_WIN.h"

@interface View_WIN ()

@end

@implementation View_WIN

- (void)viewDidLoad 
{
    [super viewDidLoad];
    
    NSLog(@"Ouverture de la fenêtre 'WIN' ! ");
    
    // ---------------------------------------
    // Lecture du fichier de sauvegarde
    
   NSString *chemin = [[self applicationDocumentsDirectory].path
                        stringByAppendingPathComponent:@"Data.txt"];
   NSString *Var_temp1 = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
    
    NSLog(@"Resultat txt: %@", Var_temp1);
    
    if (Var_temp1==NULL)
    {
        Var_temp1=@"0";
    }
    
    _best_score.text = Var_temp1;
}

- (void)setMy_Score: (int)lvl
{
    NSLog(@"Fonction set score avec : %d", lvl);
    
    NSString *variable = [@(lvl) stringValue];
    _my_score.text = variable;
    
    level = lvl;
    
UIColor *colour1 = [[UIColor alloc]initWithRed:254.0/255.0 green:233.0/255.0 blue:218.0/255.0 alpha:1.0];
UIColor *colour2 = [[UIColor alloc]initWithRed:221.0/255.0 green:240.0/255.0 blue:175.0/255.0 alpha:1.0];
UIColor *colour3 = [[UIColor alloc]initWithRed:189.0/255.0 green:243.0/255.0 blue:231.0/255.0 alpha:1.0];
UIColor *colour4 = [[UIColor alloc]initWithRed:233.0/255.0 green:204.0/255.0 blue:242.0/255.0 alpha:1.0];
UIColor *colour5 = [[UIColor alloc]initWithRed:227.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1.0];
    
    level = level-1;

    if (level < 5)
    {
        self.view.backgroundColor = colour1;
        _time_max.text = @"60";
        
        if (level > 2 && level < 5)
        {
            _time_max.text = @"25";
        }
    }
    
    else if (level>=5 && level <10)
    {
        self.view.backgroundColor = colour2; // Changement background
        _time_max.text = @"15";
    }
    
    else if (level >= 10 && level <= 15)
    {
        self.view.backgroundColor = colour3; // Changement background
        _time_max.text = [@((5-level)+20) stringValue]; // (5-lvl)+20
    }
    
    else
    {
        self.view.backgroundColor = colour4; // Changement background
        _time_max.text = @"10";
    }
    
    level = level+1;
    
    if (level >= 20)
    {
        self.view.backgroundColor = colour5;
    }
    
    
    // --------------------------------------
    // "Rate this app" DEBUGAGE RECQUIS !!!!
    
    if (level==12 || level==17)
    {
        NSString *msg = [NSString stringWithFormat:@"If you enjoy using this app, would you taking a moment to rate it ? It won't take more than a minute. Thanks for your support !"];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Rate this app :)" message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
        [alert addButtonWithTitle:@"Donner mon avis !"];
        
        [alert show];
    }
}

- (void)setMy_Time:(int)time
{
    NSLog(@"Fonction set time avec : %d", time);
    
    NSString *variable = [@(time) stringValue];
    _my_time.text = variable;
    
    if (time<10)
    {
        [_my_time setTextColor: [UIColor redColor]];
    }
    else
    {
        [_my_time setTextColor: [UIColor blackColor]];
    }
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

-(void)updateAchievements
{
    NSLog(@"updateAchivement");
    
    NSString *achievementIdentifier;
    float progressPercentage = 0.0;
    BOOL progressInLevelAchievement = NO;
    
    GKAchievement *levelAchievement = nil;
    GKAchievement *scoreAchievement = nil;
    
    // Vérification préalable
    
    NSString *contenu; // Contient le contenu du fichier "first_Rate.txt"
    NSString *chemineau = [[self applicationDocumentsDirectory].path
                           stringByAppendingPathComponent:@"first_Rate.txt"];
    contenu = [NSString stringWithContentsOfFile:chemineau encoding:NSUTF8StringEncoding error:nil];

    
        if ([contenu isEqual:@"0"] || contenu == NULL)
        {
            NSLog(@"Rate achivement ON");
            [GKNotificationBanner showBannerWithTitle:@"Rate this app :)" message:@"Thanx for your participation !" completionHandler:nil];
            progressPercentage = 100;
            achievementIdentifier = @"SUMUPS_Achivement_Rate";
            progressInLevelAchievement = YES;
            
            // Création du fichier first_Rate
            NSString *path = [[self applicationDocumentsDirectory].path
                              stringByAppendingPathComponent:@"first_Rate.txt"];
            bool rendu = [@"1" writeToFile:path atomically:YES
                                  encoding:NSUTF8StringEncoding error:nil];
            NSLog(@"Création du fichier first Rate : %d", rendu);
        }
    
    if (progressInLevelAchievement)
    {
        levelAchievement = [[GKAchievement alloc] initWithIdentifier:achievementIdentifier];
        levelAchievement.percentComplete = progressPercentage;
    }
    
    scoreAchievement = [[GKAchievement alloc] initWithIdentifier:achievementIdentifier];
    scoreAchievement.percentComplete = progressPercentage;
    
    NSArray *achievements = (progressInLevelAchievement) ? @[levelAchievement, scoreAchievement] : @[scoreAchievement];
    
    [GKAchievement reportAchievements:achievements withCompletionHandler:^(NSError *error) {
    }];
}

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        // Annuler
    }
    
    else
    {
        // Vérification des "Achivement"
        [self updateAchievements];
        
        NSLog(@"Rate this app");
        
        #define APP_ID 912056944 //id from iTunesConnect
        NSString *reviewURL = [NSString stringWithFormat:@"https://t.co/2rjigAqtJv"];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)next:(id)sender
{
    NSLog(@"Passage vers lvl up");
    
    ViewController *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    
    NSLog(@"passage du lvl: %d", level);
    [second_view Set_lvl:level];
    [self presentViewController:second_view animated:NO completion:nil];
}

- (IBAction)cancel:(id)sender
{
    NSLog(@"Passage vers menu");
    
    Menu *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    
    [self presentViewController:second_view animated:NO completion:nil];
}

@end