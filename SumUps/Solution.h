//
//  Solution.h
//  SumUps
//
//  Created by Audouin d'Aboville on 21/10/2014.
//  Copyright (c) 2014 Audouin d'Aboville. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "View_LOOSE.h"
#import "View_LOOSE_pay.h"


@interface Solution : UIViewController
{
    NSTimer *evolution;
    
    int cpt;
    int cpt2;
    int level;
    int my_goal;
    int my_button1;
    int my_button2;
    int my_button3;
    int my_button4;
    int my_button5;
    int my_button6;
    int my_button7;
    int my_button8;
    int my_button9;
    
    int my_transfert_obj[7];
}

@property (weak, nonatomic) IBOutlet UIButton *Button1;
@property (weak, nonatomic) IBOutlet UIButton *Button2;
@property (weak, nonatomic) IBOutlet UIButton *Button3;
@property (weak, nonatomic) IBOutlet UIButton *Button4;
@property (weak, nonatomic) IBOutlet UIButton *Button5;
@property (weak, nonatomic) IBOutlet UIButton *Button6;
@property (weak, nonatomic) IBOutlet UIButton *Button7;
@property (weak, nonatomic) IBOutlet UIButton *Button8;
@property (weak, nonatomic) IBOutlet UIButton *Button9;

@property (weak, nonatomic) IBOutlet UIWebView *Pub;


@property (weak, nonatomic) IBOutlet UILabel *goal;
@property (weak, nonatomic) IBOutlet UILabel *my_level;

- (void)body;
- (void)setMy_Score: (int)lvl;
- (void)setMy_Goal: (int)goal;
- (void)setMy_Board: (int)myButton1 andNum2:(int)myButton2 andNum3:(int)myButton3 andNum4:(int)myButton4 andNum5:(int)myButton5 andNum6:(int)myButton6 andNum7:(int)myButton7 andNum8:(int)myButton8 andNum9:(int)myButton9;
- (void)setMy_Solution: (int*)transfert_solution;

-(void)updateAchievements;


@end

