//
//  Menu.h
//  sumUps
//
//  Created by Audouin d'Aboville on 12/07/2014.
//  Copyright (c) 2014 Audouin d'Aboville. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "GameKit/GameKit.h"

@interface Menu : UIViewController
{
    BOOL gameCenterEnabled;
}

-(void)authenticateLocalPlayer;

- (IBAction)play:(id)sender;
- (IBAction)score:(id)sender;
- (IBAction)rules:(id)sender;

@property (weak, nonatomic) IBOutlet UIWebView *Pub;

@end
