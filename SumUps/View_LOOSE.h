//
//  View_LOOSE.h
//  sumUps
//
//  Created by Audouin d'Aboville on 12/07/2014.
//  Copyright (c) 2014 Audouin d'Aboville. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import "ViewController.h"
#import "Menu.h"
#import "Solution.h"

@interface View_LOOSE : UIViewController
{
    int level;
    int my_goal;
    int my_button1;
    int my_button2;
    int my_button3;
    int my_button4;
    int my_button5;
    int my_button6;
    int my_button7;
    int my_button8;
    int my_button9;
    int life;
    
    int my_transfert_obj[6];
}

@property (weak, nonatomic) IBOutlet UIImageView *no_life1;
@property (weak, nonatomic) IBOutlet UIImageView *no_life2;
@property (weak, nonatomic) IBOutlet UIImageView *no_life3;

@property (weak, nonatomic) IBOutlet UIImageView *life1;
@property (weak, nonatomic) IBOutlet UIImageView *life2;
@property (weak, nonatomic) IBOutlet UIImageView *life3;

@property (weak, nonatomic) IBOutlet UILabel *time_left;

- (void)setMy_Score: (int)lvl;
- (void)setMy_Goal: (int)goal;
- (void)setMy_Board: (int)myButton1 andNum2:(int)myButton2 andNum3:(int)myButton3 andNum4:(int)myButton4 andNum5:(int)myButton5 andNum6:(int)myButton6 andNum7:(int)myButton7 andNum8:(int)myButton8 andNum9:(int)myButton9;
- (void)setMy_Solution: (int*)transfert_solution;


- (IBAction)restart_clik:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *restart;


- (IBAction)Cancel_Clik:(id)sender;

- (IBAction)Solution_Clik:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *Solution;

- (IBAction)twitter_share:(id)sender;
- (IBAction)facebook_share:(id)sender;

-(void)updateAchievements:(NSString*)wich_button;

@property (weak, nonatomic) IBOutlet UILabel *Actual_score;


@end