//
//  Help.h
//  sumUps
//
//  Created by Audouin d'Aboville on 12/07/2014.
//  Copyright (c) 2014 Audouin d'Aboville. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioServices.h>

#import "ViewController.h"

@interface Help : UIViewController
{
    int Button1;
    int Button2;
    int Button3;
    int objectif;
    int variable;
    int resultat;
    
    bool pushButton1;
    bool pushButton2;
    bool pushButton3;
}

@property (weak, nonatomic) IBOutlet UIWebView *Pub_help;

@property (weak, nonatomic) IBOutlet UIButton *Label_Button1;
@property (weak, nonatomic) IBOutlet UIButton *Label_Button2;
@property (weak, nonatomic) IBOutlet UIButton *Label_Button3;

@property (weak, nonatomic) IBOutlet UILabel *goal;

- (IBAction)action1:(id)sender;
- (IBAction)action2:(id)sender;
- (IBAction)action3:(id)sender;

- (void)audio_tools:(NSString*)which_one;
-(void)updateAchievements;

@end
