//
//  Help.m
//  sumUps
//
//  Created by Audouin d'Aboville on 12/07/2014.
//  Copyright (c) 2014 Audouin d'Aboville. All rights reserved.
//

#import "Help.h"

@interface Help ()

@end

@implementation Help

- (void)viewDidLoad 
{
    [super viewDidLoad];
    
    NSLog(@"Passage vue aide ! ");
    
    //  Espace publicitaire & google analytics
    
    [_Pub_help loadRequest:[NSURLRequest requestWithURL: [NSURL URLWithString : @"http://www.ad-inc.fr/Autres/SumUp/help.html"]]];
    
    NSLog(@"Analytics init ok");
    
    // Initialisation
    
    Button1=1;
    Button2=2;
    Button3=2;
    objectif=5;
    resultat=0;
    
    pushButton1=false;
    pushButton2=false;
    pushButton3=false;
    
    // Conversion d'affichage
    
    NSString *temporaire = [@(Button1) stringValue];
    [_Label_Button1 setTitle: temporaire forState:UIControlStateNormal];
    
    temporaire = [@(Button2) stringValue];
    [_Label_Button2 setTitle: temporaire forState:UIControlStateNormal];
    
    temporaire = [@(Button3) stringValue];
    [_Label_Button3 setTitle: temporaire forState:UIControlStateNormal];
    
    temporaire = [@(0) stringValue];
    _goal.text = temporaire;
    
    // Colorisation de l'affichage
    
    UIColor *colourButton = [[UIColor alloc]initWithRed:0.0/255.0 green:51.0/255.0 blue:102.0/255.0 alpha:1.0];
    [_Label_Button1 setTintColor: colourButton];
    [_Label_Button2 setTintColor: colourButton];
    [_Label_Button3 setTintColor: colourButton];
    
    [_Label_Button1 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
    [_Label_Button2 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
    [_Label_Button3 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
    
    // Message d'aide
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Help & clues !" message:@"You have only one minute to reach the goal by mathematics and your head. You can use different combinaisons." delegate:self cancelButtonTitle:@"Continue" otherButtonTitles:nil];
    [alert addButtonWithTitle:@"Quit"];
    
    [alert show];
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

- (void)audio_tools:(NSString*)which_one
{
    NSLog(@"Sound !");
    
    if ([which_one isEqual:@"loose"])
    {
        SystemSoundID loose;
        
        AudioServicesCreateSystemSoundID(CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR("loose"), CFSTR("caf"), NULL), &loose);
        AudioServicesPlaySystemSound(loose);
    }
    
    else if ([which_one isEqual:@"win"])
    {
        SystemSoundID winn;
        
        AudioServicesCreateSystemSoundID(CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR("winn"), CFSTR("caf"), NULL), &winn);
        AudioServicesPlaySystemSound(winn);
    }
    
    else if ([which_one isEqual:@"clique"])
    {
        SystemSoundID clique;
        
        AudioServicesCreateSystemSoundID(CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR("Clique"), CFSTR("caf"), NULL), &clique);
        AudioServicesPlaySystemSound(clique);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        NSLog(@"Continuue");
        // [self viewDidLoad]; // Reboot la view ou ne fait rien *** DEBUG ICI
    }
    
    else
    {
        // Retour à l'accueil
        
        NSLog(@"Passage vers menu");
        
        Menu *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
        
        [self presentViewController:second_view animated:NO completion:nil];
    }
}

-(void)updateAchievements
{
    NSLog(@"updateAchivement");
    
    NSString *achievementIdentifier;
    float progressPercentage = 0.0;
    BOOL progressInLevelAchievement = NO;
    
    GKAchievement *levelAchievement = nil;
    GKAchievement *scoreAchievement = nil;
    
    // Vérification préalable
    
    NSString *contenu; // Contient le contenu du fichier "first_Help.txt"
    NSString *chemineau = [[self applicationDocumentsDirectory].path
                           stringByAppendingPathComponent:@"first_Help.txt"];
    contenu = [NSString stringWithContentsOfFile:chemineau encoding:NSUTF8StringEncoding error:nil];
    
    
    if ([contenu isEqual:@"0"] || contenu == NULL)
    {
        NSLog(@"Help achivement ON");
        [GKNotificationBanner showBannerWithTitle:@"Help and Clues !" message:@"You know how to play now !" completionHandler:nil];
        progressPercentage = 100;
        achievementIdentifier = @"SUMUPS_Achivement_Help";
        progressInLevelAchievement = YES;
        
        // Création du fichier first_Help
        NSString *path = [[self applicationDocumentsDirectory].path
                          stringByAppendingPathComponent:@"first_Help.txt"];
        bool rendu = [@"1" writeToFile:path atomically:YES
                              encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"Création du fichier first Help : %d", rendu);
    }
    
    if (progressInLevelAchievement)
    {
        levelAchievement = [[GKAchievement alloc] initWithIdentifier:achievementIdentifier];
        levelAchievement.percentComplete = progressPercentage;
    }
    
    scoreAchievement = [[GKAchievement alloc] initWithIdentifier:achievementIdentifier];
    scoreAchievement.percentComplete = progressPercentage;
    
    NSArray *achievements = (progressInLevelAchievement) ? @[levelAchievement, scoreAchievement] : @[scoreAchievement];
    
    [GKAchievement reportAchievements:achievements withCompletionHandler:^(NSError *error) {
    }];
}

- (IBAction)action1:(id)sender
{
    [self audio_tools:@"clique"];
    
    if (pushButton1==false)
    {
        UIColor *colourBlue = [[UIColor alloc]initWithRed:63.0/255.0 green:75.0/255.0 blue:110.0/255.0 alpha:1.0];
        [_Label_Button1 setTintColor : colourBlue];
        [_Label_Button1 setBackgroundImage:[UIImage imageNamed:@"posfocused.png"] forState:UIControlStateNormal];
    pushButton1=true;
        resultat=resultat+Button1;
    }
    else
    {
        UIColor *colourButton = [[UIColor alloc]initWithRed:0.0/255.0 green:51.0/255.0 blue:102.0/255.0 alpha:1.0];
        [_Label_Button1 setTintColor: colourButton];
        
        [_Label_Button1 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
    pushButton1=false;
        resultat=resultat-Button1;
    }
    
    NSString *variablee = [@(resultat) stringValue];
    _goal.text = variablee;
    
    if (resultat==objectif)
    {
        NSLog(@"WIN");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Well done!!" message:@"You win ! The tutorial is finished !" delegate:self cancelButtonTitle:@"Restart" otherButtonTitles:nil];
        [alert addButtonWithTitle:@"Quit"];
        
        [alert show];
        
        // Vérification des "Achivement"
        [self updateAchievements];
    }
}

- (IBAction)action2:(id)sender
{
    [self audio_tools:@"clique"];
    
    if (pushButton2==false)
    {
        UIColor *colourBlue = [[UIColor alloc]initWithRed:63.0/255.0 green:75.0/255.0 blue:110.0/255.0 alpha:1.0];
        [_Label_Button2 setTintColor : colourBlue];
        [_Label_Button2 setBackgroundImage:[UIImage imageNamed:@"posfocused.png"] forState:UIControlStateNormal];
        pushButton2=true;
        resultat=resultat+Button2;
    }
    else
    {
        UIColor *colourButton = [[UIColor alloc]initWithRed:0.0/255.0 green:51.0/255.0 blue:102.0/255.0 alpha:1.0];
        [_Label_Button2 setTintColor: colourButton];
        [_Label_Button2 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
        pushButton2=false;
        resultat=resultat-Button2;
    }
    
    NSString *variabl = [@(resultat) stringValue];
    _goal.text =variabl;
    
    if (resultat==objectif)
    {
        NSLog(@"WIN");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Well done!!" message:@"You win ! The tutorial is finished !" delegate:self cancelButtonTitle:@"Restart" otherButtonTitles:nil];
        [alert addButtonWithTitle:@"Quit"];
        
        [alert show];
     
        // Vérification des "Achivement"
        [self updateAchievements];
    }
}

- (IBAction)action3:(id)sender
{
    [self audio_tools:@"clique"];
    
    if (pushButton3==false)
    {
         UIColor *colourBlue = [[UIColor alloc]initWithRed:63.0/255.0 green:75.0/255.0 blue:110.0/255.0 alpha:1.0];
        [_Label_Button3 setTintColor : colourBlue];
        [_Label_Button3 setBackgroundImage:[UIImage imageNamed:@"posfocused.png"] forState:UIControlStateNormal];
        pushButton3=true;
        resultat=resultat+Button3;
    }
    else
    {
        UIColor *colourButton = [[UIColor alloc]initWithRed:0.0/255.0 green:51.0/255.0 blue:102.0/255.0 alpha:1.0];
        [_Label_Button3 setTintColor: colourButton];
        [_Label_Button3 setBackgroundImage:[UIImage imageNamed:@"posbut - copie.png"] forState:UIControlStateNormal];
        pushButton3=false;
        resultat=resultat-Button3;
    }
    
    NSString *variabe = [@(resultat) stringValue];
    _goal.text =variabe;
    
    if (resultat==objectif)
    {
        NSLog(@"WIN");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Well done!!" message:@"You win ! The tutorial is finished !" delegate:self cancelButtonTitle:@"Restart" otherButtonTitles:nil];
        [alert addButtonWithTitle:@"Quit"];
        
        [alert show];
        
        // Vérification des "Achivement"
        [self updateAchievements];
    }
}

@end