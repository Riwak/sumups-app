//
//  View_WIN.h
//  sumUps
//
//  Created by Audouin d'Aboville on 12/07/2014.
//  Copyright (c) 2014 Audouin d'Aboville. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "Menu.h"

@interface View_WIN : UIViewController
{
    int level;
}

- (IBAction)next:(id)sender;
- (IBAction)cancel:(id)sender;

-(void)updateAchievements;

@property (weak, nonatomic) IBOutlet UILabel *my_score;
@property (weak, nonatomic) IBOutlet UILabel *my_time;
@property (weak, nonatomic) IBOutlet UILabel *best_score;
@property (weak, nonatomic) IBOutlet UILabel *time_max;


- (void)setMy_Score: (int)lvl;
- (void)setMy_Time: (int)time;

@end
