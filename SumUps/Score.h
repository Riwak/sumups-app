//
//  Score.h
//  sumUps
//
//  Created by Audouin d'Aboville on 12/07/2014.
//  Copyright (c) 2014 Audouin d'Aboville. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import "GameKit/GameKit.h"
#import "Menu.h"

@interface Score : UIViewController <GKGameCenterControllerDelegate>
{
    
}

- (IBAction)backtomenu:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *Label_BestScore;
@property (weak, nonatomic) IBOutlet UIWebView *Pub_Score;

@property (weak, nonatomic) IBOutlet UIImageView *lvl2;
@property (weak, nonatomic) IBOutlet UIImageView *lvl3;
@property (weak, nonatomic) IBOutlet UIImageView *lvl4;

@property (weak, nonatomic) IBOutlet UIImageView *silver1;
@property (weak, nonatomic) IBOutlet UIImageView *silver2;
@property (weak, nonatomic) IBOutlet UIImageView *silver3;

- (IBAction)BouttonFacebook:(id)sender;
- (IBAction)BouttonTwitter:(id)sender;

- (IBAction)show_leaderboard:(id)sender;
-(void)updateAchievements:(NSString*)wich_button;
-(void)resetAchievements;
-(void)showLeaderboardAndAchievements:(BOOL)shouldShowLeaderboard;

@end
