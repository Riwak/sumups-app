//
//  Menu.m
//  sumUps
//
//  Created by Audouin d'Aboville on 12/07/2014.
//  Copyright (c) 2014 Audouin d'Aboville. All rights reserved.
//

#import "Menu.h"

@interface Menu ()

@end

@implementation Menu

- (void)viewDidLoad 
{
    [super viewDidLoad];
    
    //  Espace publicitaire & google analytics
    
    [_Pub loadRequest:[NSURLRequest requestWithURL: [NSURL URLWithString : @"http://www.ad-inc.fr/Autres/SumUp/menu.html"]]];
    
    NSLog(@"Analytics init ok");
    
    //  Authentification du joueur auprès de GameCenter
    
    [self authenticateLocalPlayer];
    
    // Vérification du fichier date
     // --------------------------------------------------------------------
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy.MM.dd HH:mm:ss"];
    
    // [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSString *content; // Contient le contenu du fichier "Date.txt"
    
    NSString *chemin = [[self applicationDocumentsDirectory].path
                           stringByAppendingPathComponent:@"Date.txt"];
    content = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
    
    
    if (content == NULL) // Fichier date vide
    {
        //  Je prend ma current date je la convertie et je la met dans le fichier date.txt
        NSDate *start_game = [NSDate date];
        NSString *text = [formatter stringFromDate:start_game];
        
        [text writeToFile:chemin atomically:YES encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"Date upgrade (bool), %@", text);
    }
    else
    {
        // Je vais chercher ma date dans le fichier txt
        NSString *Date_du_fichier = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"Resultat date: %@", Date_du_fichier);
        
        NSDate *dateFromString = [formatter dateFromString:Date_du_fichier]; // COnvertion
        NSLog(@"Date après convertion en date : %@", dateFromString);
        
        NSTimeInterval timeInterval = [dateFromString timeIntervalSinceNow]; // Interval entre d et fichier
        NSLog(@"Time Interval : %f", timeInterval);
        
       if (timeInterval < -7200)
       {
          [GKNotificationBanner showBannerWithTitle:@"New lives !" message:@"Yeah ! You have 3 new lives to spend !" completionHandler:nil];
           
           NSLog(@"Time interval < -7200");
           NSLog(@"Remise à niveau du fichier de vie");
    
           [@"3" writeToFile:[[self applicationDocumentsDirectory].path stringByAppendingPathComponent:@"Life.txt"] atomically:YES encoding:NSUTF8StringEncoding error:nil];
           NSLog(@"Life upgrade (bool), 3");
           
           // Ensuite je met une nouvelle date dans le fichier
           NSDate *start_game = [NSDate date];
           NSString *text = [formatter stringFromDate:start_game];
           [text writeToFile:chemin atomically:YES encoding:NSUTF8StringEncoding error:nil];
           NSLog(@"Date upgrade (bool), %@", text);
       }
        else
        {
            NSLog(@"DO nothing");
            NSLog(@"Il reste : %f /-7200", timeInterval);
        }
    }
    
    
    //  -----------------------------------------------------------
    //  Vérification du fichier de vie utilisateur & INITIALISATION
    
    NSString *contenu; // Contient le contenu du fichier "Life.txt"
    
    NSString *chemineau = [[self applicationDocumentsDirectory].path
                           stringByAppendingPathComponent:@"Life.txt"];
    contenu = [NSString stringWithContentsOfFile:chemineau encoding:NSUTF8StringEncoding error:nil];
    
    if (contenu == NULL)
    {
        NSString* texte = @"3";
        
        // Ecriture
        NSString *path = [[self applicationDocumentsDirectory].path stringByAppendingPathComponent:@"Life.txt"];
        [texte writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"Life upgrade (bool), %@", texte);
        
        [GKNotificationBanner showBannerWithTitle:@"New lives !" message:@"Yeah ! You have 3 new lives to spend !" completionHandler:nil];
    }
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

-(void)authenticateLocalPlayer
{
    NSLog(@"authenticateLocalPlayer ok");
    
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        if (viewController != nil) {
            [self presentViewController:viewController animated:YES completion:nil];
        }
        else{
            if ([GKLocalPlayer localPlayer].authenticated) {
                gameCenterEnabled = YES;
                
                // Get the default leaderboard identifier.
                [[GKLocalPlayer localPlayer] loadDefaultLeaderboardIdentifierWithCompletionHandler:^(NSString *leaderboardIdentifier, NSError *error) {
                    
                    if (error != nil) {
                        NSLog(@"%@", [error localizedDescription]);
                    }
                    else{
                        leaderboardIdentifier = @"SUMUPS_LEADERBOARDS";
                    }
                }];
            }
            
            else{
                gameCenterEnabled = NO;
            }
        }
    };
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)play:(id)sender
{
    NSLog(@"Passage vers ViewController");
    
    ViewController *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    
    second_view.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:second_view animated:YES completion:nil];
}

- (IBAction)score:(id)sender
{
    NSLog(@"Passage vers ViewScore");
    
    Score *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"Score"];
    
    second_view.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:second_view animated:YES completion:nil];
}

- (IBAction)rules:(id)sender
{
    NSLog(@"Passage vers ViewHelp");
    
    Help *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"Help"];
    
    second_view.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:second_view animated:YES completion:nil];
}

@end