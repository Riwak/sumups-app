# What is SumUps #

Ready for a new challenge ?

SumsUps is a new intuitive addictive maths game which permits you to enhance your calculation capacities. Add up numbers before time runs out.

Do you have what it takes ? Find your breaking point !!
What will your best score be ?

### Version ###

* V1.4 iOS
* V1.0 Android 

### Team ###

* Réalisation iOS : Audouin d'Aboville.
* Réalisation Android : Laszlo de Brissac.

### Links ###

* WebLink : http://ad-inc.pagesperso-orange.fr/SumUp/
* Facebook Link : https://www.facebook.com/sumups?fref=ts
* App Link (iOS) : https://itunes.apple.com/us/app/sumups/id912056944?l=fr&ls=1&mt=8
* App Link (Android) : https://play.google.com/store/apps/details?id=com.firstapp.sumup&hl=en